#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GClusterAlgorithm.h"
#import "GClusterRenderer.h"
#import "GQTPointQuadTreeItem.h"

@interface GClusterManager : NSObject

@property(nonatomic, strong) GMSMapView *mapView;
@property(nonatomic, strong) id<GClusterAlgorithm> clusterAlgorithm;
@property(nonatomic, strong) id<GClusterRenderer> clusterRenderer;

- (void)addItem:(id<GClusterItem>)item;
- (void)removeItem:(id<GClusterItem>)item;
- (void)removeItems;

- (void)clusterWithForceRerender:(BOOL)forceRerender;

//convenience

+ (instancetype)managerWithMapView:(GMSMapView*)googleMap
                         algorithm:(id<GClusterAlgorithm>)algorithm
                          renderer:(id<GClusterRenderer>)renderer;

@end
