#import <CoreText/CoreText.h>
#import "GDefaultClusterRenderer.h"
#import "GQuadItem.h"
#import "GCluster.h"
#import "GClusterItem.h"
#import "MapSpot.h"
#import "Utility.h"

@implementation GDefaultClusterRenderer {
    GMSMapView *_map;
    NSMutableArray *_markerCache;
    NSInteger maxClusterRenderedCount;
    
    float zoomLevel;
    BOOL isAllClustersRendered;
    CLLocationCoordinate2D currentCenter;
    double maxDistance;
}

- (id)initWithMapView:(GMSMapView*)googleMap {
    if (self = [super init]) {
        _map = googleMap;
        _markerCache = [[NSMutableArray alloc] init];
        maxDistance = 0;
        zoomLevel = -1;
        isAllClustersRendered = NO;
        maxClusterRenderedCount = 300;
    }
    return self;
}

- (void)setMaxClusterRenderedCount:(NSInteger)newCount
{
    maxClusterRenderedCount = newCount;
}

- (void)clustersChanged:(NSSet*)clusters andForceRerender:(BOOL)forceRender {
    
    if(zoomLevel == _map.camera.zoom && !forceRender)
    {
        if(![self needRender] || [clusters count]<=0 || isAllClustersRendered)
        {
            return;
        }
    }
    else
    {
        // If zoomLevel changed, always rerender map and save current zoom level
        zoomLevel = _map.camera.zoom;
    }
    
    [Utility debug:@"====== Rerender Map !! ======"];
    
    for (GMSMarker *marker in _markerCache) {
        marker.map = nil;
    }
    
    [_markerCache removeAllObjects];
    
    GMSProjection* projection = [_map projection];
    
    CLLocationCoordinate2D topLeft = projection.visibleRegion.farLeft;
    CLLocationCoordinate2D bottomRight = projection.visibleRegion.nearRight;
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((topLeft.latitude + bottomRight.latitude) / 2, (topLeft.longitude + bottomRight.longitude) / 2);
    
    NSArray* sortedClusters = [self sortClustersByDistance:clusters withCenterLocation:center];
    
    // For find the max size of showed clusters
    NSInteger maxClusterSize = 1;
    for (id<GCluster> cluster in sortedClusters)
    {
        if(![projection containsCoordinate:cluster.position] && [_markerCache count]>maxClusterRenderedCount)
        {
            break;
        }
        
        if([cluster.items count] > maxClusterSize)
            maxClusterSize = [cluster.items count];
    }
    
    // To render the clusters
    for (id<GCluster> cluster in sortedClusters)
    {
        if(![projection containsCoordinate:cluster.position] && [_markerCache count]>maxClusterRenderedCount)
        {
            break;
        }
        
        GMSMarker *marker;
        NSUInteger count = cluster.items.count;
        
//        if(count == 14)
//        {
//            for(id<GCluster> c in cluster.items)
//            {
//                id<GCluster> tmp = c;
//                
//                while(![tmp conformsToProtocol:@protocol(GClusterItem)] && tmp.items.count>0)
//                {
//                    for(id<GCluster> cc in tmp.items)
//                    {
//                        tmp = cc;
//                        break;
//                    }
//                }
//                
//                if([tmp conformsToProtocol:@protocol(GClusterItem)]) {
//                    
//                    //NSLog(@"XXXX : %f %f", tmp.position.latitude, tmp.position.longitude);
//                    MapSpot* spot = tmp;
//                    Temple* t = spot.marker.userData;
//                    NSLog(@"=====> %@ %f %f",
//                    t.chName, t.latitude, t.longitude
//                          );
//                }
//            }
//        }
//        else
        
        if (count > 1) {
            
            float weight = log10f(count*10/(float)maxClusterSize) * (zoomLevel > 8 ? 0.75 : 1);
            if(weight < 0) weight = 0;
            
            marker = [[GMSMarker alloc] init];
            marker.position = cluster.position;
            marker.icon = [self generateClusterIconWithCount:count andWeight:weight];
        }
        else
        {
            BOOL isFind = NO;
            id<GCluster> c = cluster;
            
            while(!isFind)
            {
                for(id ci in c.items)
                {
                    if([ci conformsToProtocol:@protocol(GClusterItem)]) // find item
                    {
                        MapSpot* spot = ci;
                        marker = spot.marker;
                        isFind = YES;
                    }
                    else
                    {
                        c = ci;
                    }
                    
                    break;
                }
            }
        }
        
        if(marker) {
        [_markerCache addObject:marker];
        marker.map = _map;
        }
    }
    
    // Update maxDistance and center location
    if([_markerCache count] > 0)
    {
        GMSMarker* lastMarker = [_markerCache lastObject];
        maxDistance = [self distanceBetween:lastMarker.position and:center];
        currentCenter = center;
        
        [Utility debug:@"maxDistance = %f", maxDistance];
        [Utility debug:@"maxLocation = (%f, %f)", lastMarker.position.latitude, lastMarker.position.longitude];
        [Utility debug:@"center = (%f, %f)", currentCenter.latitude, currentCenter.longitude];
        [Utility debug:@"Total cluster count = %d", [clusters count]];
        [Utility debug:@"Cluster count rendered = %d", [_markerCache count]];
    }
    
    if([clusters count] == [_markerCache count])
    {
        isAllClustersRendered = YES;
    }
    else
    {
        isAllClustersRendered = NO;
    }
}

- (NSArray*)sortClustersByDistance:(NSSet*)clusters withCenterLocation:(CLLocationCoordinate2D)center
{
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES comparator:^NSComparisonResult(id obj1, id obj2)
        {
            id<GCluster> c1 = (id<GCluster>) obj1;
            id<GCluster> c2 = (id<GCluster>) obj2;
            
            CLLocationCoordinate2D p1 = c1.position;
            CLLocationCoordinate2D p2 = c2.position;
            
            double d1 = [self distanceBetween:p1 and:center];
            double d2 = [self distanceBetween:p2 and:center];
            
            if(d1 > d2)
            {
                return NSOrderedDescending;
            }
            else if(d1 < d2)
            {
                return NSOrderedAscending;
            }
            
            return NSOrderedSame;
        }];
    
    NSArray* sortedClusters = [clusters sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sortDescriptor, nil]];
    
#if 0//DEBUG
    // Print sorted clusters
    for(id<GCluster> c in sortedClusters)
    {
        CLLocationCoordinate2D p2 = c.position;
        double d = (p2.latitude - center.latitude) * (p2.latitude - center.latitude) + (p2.longitude - center.longitude) * (p2.longitude - center.longitude);
        [Utility debug:@"cluster distance = %f (size: %d)", d, [c.items count]];
    }
#endif
    
    return sortedClusters;
}

- (double)distanceBetween:(CLLocationCoordinate2D)p1 and:(CLLocationCoordinate2D)p2
{
    return (p1.latitude - p2.latitude) * (p1.latitude - p2.latitude) + (p1.longitude - p2.longitude) * (p1.longitude - p2.longitude);
}

- (BOOL)needRender
{
    if(maxDistance <= 0)
        return YES;
    
    GMSProjection* projection = _map.projection;
    double d;

    d = [self distanceBetween:currentCenter and:projection.visibleRegion.farLeft];
    
    if(d > maxDistance)
        return YES;
    
    d = [self distanceBetween:currentCenter and:projection.visibleRegion.farRight];
    
    if(d > maxDistance)
        return YES;

    d = [self distanceBetween:currentCenter and:projection.visibleRegion.nearLeft];
    
    if(d > maxDistance)
        return YES;
    
    d = [self distanceBetween:currentCenter and:projection.visibleRegion.nearRight];
    
    if(d > maxDistance)
        return YES;
    
    return NO;
}

- (UIImage*) generateClusterIconWithCount:(NSUInteger)count andWeight:(float)weight {
    
    float inset = 3;
    
    int diameter = (int) (68 * weight);
    if(diameter < 33) diameter = 33;
    
    CGRect rect = CGRectMake(0, 0, diameter, diameter);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);

    CGContextRef ctx = UIGraphicsGetCurrentContext();

    // set stroking color and draw circle
    [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.8] setStroke];
    
    // background color
    UIColor *bgColor = [UIColor colorWithHue:(0.75-weight*0.75) saturation:1.000 brightness:0.782 alpha:1.000];
    [bgColor setFill];

    CGContextSetLineWidth(ctx, inset);

    // make circle rect 5 px from border
    CGRect circleRect = CGRectMake(0, 0, diameter, diameter);
    circleRect = CGRectInset(circleRect, inset, inset);

    // draw circle
    CGContextFillEllipseInRect(ctx, circleRect);
    CGContextStrokeEllipseInRect(ctx, circleRect);

    CGFloat fontSize = 22.0 * weight;
    if(fontSize < 13) fontSize = 13.0;
    
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)@"Helvetica-Bold", fontSize, NULL);
    
    NSDictionary *attributesDict = [NSDictionary dictionaryWithObjectsAndKeys:
            (__bridge id)myFont, (id)kCTFontAttributeName,
                    [UIColor whiteColor], (id)kCTForegroundColorAttributeName, nil];

    // create a naked string
    NSString *string = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)count];

    NSAttributedString *stringToDraw = [[NSAttributedString alloc] initWithString:string
                                                                       attributes:attributesDict];

    // flip the coordinate system
    CGContextSetTextMatrix(ctx, CGAffineTransformIdentity);
    CGContextTranslateCTM(ctx, 0, diameter);
    CGContextScaleCTM(ctx, 1.0, -1.0);

    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)(stringToDraw));
    CGSize suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(
                                                                        frameSetter, /* Framesetter */
                                                                        CFRangeMake(0, stringToDraw.length), /* String range (entire string) */
                                                                        NULL, /* Frame attributes */
                                                                        CGSizeMake(diameter, diameter), /* Constraints (CGFLOAT_MAX indicates unconstrained) */
                                                                        NULL /* Gives the range of string that fits into the constraints, doesn't matter in your situation */
                                                                        );
    CFRelease(frameSetter);
    
    //Get the position on the y axis
    float midHeight = (diameter - suggestedSize.height + 6) / 2;
    float midWidth = (diameter - suggestedSize.width) / 2;

    CTLineRef line = CTLineCreateWithAttributedString(
            (__bridge CFAttributedStringRef)stringToDraw);
    CGContextSetTextPosition(ctx, midWidth, midHeight);
    CTLineDraw(line, ctx);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

@end
