//
//  MTReachabilityManager.h
//  TaiwanTemple
//
//  Created by yalight on 2014/10/26.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface MTReachabilityManager : NSObject

@property (strong, nonatomic) Reachability *reachability;

#pragma mark -
#pragma mark Shared Manager
+ (MTReachabilityManager *)sharedManager;

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end
