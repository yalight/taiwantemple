//
//  AppDelegate.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/1.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

