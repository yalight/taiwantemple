//
//  AppDelegate.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/1.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

#import "AppDelegate.h"
#import "Utility.h"
#import "MTReachabilityManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
            

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [GMSServices provideAPIKey:@"AIzaSyAwkixqGa8LmAWuvpeDQQCNw0_ybuhnVkk"];
    [self initializeStoryBoardBasedOnScreenSize];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self networkReachabilityTest];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self networkReachabilityTest];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)initializeStoryBoardBasedOnScreenSize {
    
    [MTReachabilityManager isReachable];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {    // The iOS device = iPhone or iPod Touch
        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        [Utility debug:[NSString stringWithFormat:@"Screen height: %f", iOSDeviceScreenSize.height]];
        
        if (iOSDeviceScreenSize.height >= 568)
        {   // iPhone 5 and iPod Touch 5th generation: 4 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            UIStoryboard *iPhone5Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UIStoryboard *iPhone5Storyboard = [UIStoryboard storyboardWithName:@"Main-iphone5" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone5Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }
        else//if (iOSDeviceScreenSize.height == 480)
        {   // iPhone 3GS, 4, and 4S and iPod Touch 3rd and 4th generation: 3.5 inch screen (diagonally measured)
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone35
            UIStoryboard *iPhone4Storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone4Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }
        
    }
//    else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//    {   // The iOS device = iPad
//        
//        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
//        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
//        splitViewController.delegate = (id)navigationController.topViewController;
//        
//    }
}

- (void)networkReachabilityTest
{
    static UIAlertView* alertView;
    
    if(alertView)
    {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
    
    BOOL reachable = [MTReachabilityManager isReachable];
    
    if(!reachable)
    {
        alertView = [[UIAlertView alloc] initWithTitle:@"網路連線錯誤"
                                                   message:@"請檢查您的網路連線是否可正常使用。"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                         otherButtonTitles:@"確定", nil];
        alertView.tag = 400;
        
        [alertView show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [Utility debug:NSStringFromSelector(_cmd)];
    
    if(alertView.tag == 400)
    {
        if(buttonIndex == 1)
            [self networkReachabilityTest];
    }
}

@end
