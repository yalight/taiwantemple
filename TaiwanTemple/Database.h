//
//  Database.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/12.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "Utility.h"
#import "Temple.h"
#import "TempleStructure.h"

@interface Database : NSObject

@property (strong, nonatomic) FMDatabaseQueue* fmdbq;

+(instancetype)sharedInstance;

- (void)openDatabase;
- (void)createDatabase;
- (void)removeDatabase;
- (void)insertTempleRecord:(Temple*)temple;
- (Temple*)getTempleWithId:(NSUInteger)tid;
- (NSArray*)getTemplesWithReligion:(NSString*)religion validAddress:(NSInteger)validAddress;
- (NSMutableArray*)getTempleColumnGroupByColumn:(NSString*)columnName where:(NSString*)where orderBy:(NSString*)orderBy;
- (NSArray*)getTempleWithDistrict:(NSString*)district andReligion:(NSString*)religion andDeity:(NSString*)deity andKeyword:(NSString*)keyword;
- (NSInteger)getTempleCountWithDistrict:(NSString*)district andReligion:(NSString*)religion andDeity:(NSString*)deity andKeyword:(NSString*)keyword;
- (NSMutableArray*)getFavoriteTemples;
- (void)updateIntegerColumn:(NSString*)columnName withId:(NSUInteger)tid andValue:(NSInteger)newValue;
- (void)updateFavoriteWithId:(NSUInteger)tid andValue:(NSInteger)newValue;
- (void)updateValidAddressWithId:(NSUInteger)tid andValue:(int)newValue;
- (void)updateCoordinate:(NSUInteger)tid toLatitude:(double)latitude andLongitude:(double)longitude;
- (Temple*)convertResultSetToTemple:(FMResultSet*)rs;

- (NSMutableArray*)getAllTempleStructures;
- (TempleStructure*)convertResultSetToTempleStructure:(FMResultSet*)rs;

- (void)fixData;

@end