//
//  Database.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/12.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Database.h"

#define COPY_DB_FROM_BUNDLE 0

@interface Database ()
@end

@implementation Database

#pragma mark - init

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - Singleton methods

+ (instancetype)sharedInstance
{
    static Database* sharedDatabase = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedDatabase = [[self alloc] init];
    });
    
    return sharedDatabase;
}

#pragma mark - private methods

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSError *error = nil;
    //設定這個URL指向的資料為不備份到iCloud
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: &error];
    return success;
}


- (NSString*)getDatabasePath
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbDirectoryPath = [documentDirectory stringByAppendingPathComponent:@"database"];
    NSString *dbPath = [dbDirectoryPath stringByAppendingPathComponent:@"TaiwanTemple.db"];
    
    // create directory if it is not existed
    if (![[NSFileManager defaultManager] fileExistsAtPath:dbDirectoryPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dbDirectoryPath withIntermediateDirectories:NO attributes:nil error:&error];
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:dbDirectoryPath]];
    }

    [Utility debug:@"DB Path = %@", dbPath];
    
    return dbPath;
}

- (BOOL)copyDbFromBundleToDocument
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    NSString* dbPath = [self getDatabasePath];
    NSString *dbBundlePath = [[NSBundle mainBundle] pathForResource:@"TaiwanTemple" ofType:@"db"];
    NSError* error;
    [[NSFileManager defaultManager] copyItemAtPath:dbBundlePath toPath:dbPath error:&error];
    
    if(error)
    {
        [Utility error:@"Copy DB Fail : %@", [error localizedDescription]];
        return NO;
    }
    
    return YES;
}

- (NSMutableString*)makeWhereConditionWithDistrict:(NSString*)district andReligion:(NSString*)religion andDeity:(NSString*)deity andKeyword:(NSString*)keyword
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    NSMutableString* whereCondition = [[NSMutableString alloc] initWithString:@"1=1"];
    
    if(district)
    {
        [whereCondition appendFormat:@" AND district='%@'", district];
    }
    
    if(religion)
    {
        if([@"NULL" isEqualToString:religion])
        {
            [whereCondition appendString:@" AND religion IS NULL"];
        }
        else
        {
            [whereCondition appendFormat:@" AND religion='%@'", religion];
        }
    }
    
    if(deity)
    {
        if([@"NULL" isEqualToString:deity])
        {
            [whereCondition appendFormat:@" AND deity IS NULL"];
        }
        else
        {
            [whereCondition appendFormat:@" AND deity='%@'", deity];
        }
    }
    
    if(keyword)
    {
        [keyword stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(keyword.length > 0)
        {
            [whereCondition appendString:[NSString stringWithFormat:@" AND (ch_name LIKE ? OR en_name LIKE ?)"]];
        }
    }
    
    return whereCondition;
}

#pragma mark - Database operation methods

- (void)openDatabase
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if(_fmdbq)
        return;
    
    NSString *dbPath = [self getDatabasePath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:dbPath])
    {
        if(![self copyDbFromBundleToDocument])
        {
            [Utility error:@"Open database fail"];
            return;
        }
    }
    
    _fmdbq = [FMDatabaseQueue databaseQueueWithPath:dbPath];
}

- (void)createDatabase
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return;
    }
    
    NSString *dbPath = [self getDatabasePath];

    _fmdbq = [FMDatabaseQueue databaseQueueWithPath:dbPath];
    
    [_fmdbq inDatabase:^(FMDatabase *db)
     {
         [db executeUpdate:@"CREATE TABLE IF NOT EXISTS temples (id INTEGER PRIMARY KEY AUTOINCREMENT, seq TEXT, ch_name TEXT, en_name TEXT, deity TEXT, district TEXT, orig_address TEXT, address TEXT, latitude REAL, longitude REAL, religion TEXT, tel TEXT, charge_person TEXT, valid_address INTEGER DEFAULT 10, favorite INTEGER DEFAULT 5)"];
     }];
}

- (void)removeDatabase
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    NSString *dbPath = [self getDatabasePath];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:dbPath])
    {
        [Utility info:@"Remove DB"];
        [[NSFileManager defaultManager] removeItemAtPath:dbPath error:nil];
    }
}

- (void)insertTempleRecord:(Temple*)temple
{
    [Utility debug:@"%@ => %@", NSStringFromSelector(_cmd), [temple toString]];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return;
    }
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        [db executeUpdate:@"INSERT INTO temples(seq, ch_name, en_name, deity, district, orig_address, address, latitude, longitude, religion, tel, charge_person, valid_address) VALUES (:seq, :ch_name, :en_name, :deity, :district, :orig_address, :address, :latitude, :longitude, :religion, :tel, :charge_person, :valid_address)", temple.seq, temple.chName, temple.enName, temple.deity, temple.district, temple.origAddress, temple.address, [NSNumber numberWithDouble:temple.latitude], [NSNumber numberWithDouble:temple.longitude], temple.religion, temple.tel, temple.chargePerson, [NSNumber numberWithInteger:temple.validAddress]];
    }];
}

- (Temple*)getTempleWithId:(NSUInteger)tid
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    __block Temple* temple;
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        FMResultSet* rs = [db executeQuery:@"SELECT * FROM temples WHERE id=?", [NSNumber numberWithInteger:tid]];
        
        if(![rs next])
        {
            [Utility warn:@"%@ : no query result", NSStringFromSelector(_cmd)];
            return;
        }
        
        temple = [self convertResultSetToTemple:rs];
        
        [rs close];
    }];
    
    return temple;
}

- (NSArray*)getAllTemples:(NSInteger)validAddress
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    __block NSMutableArray* temples = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        
        FMResultSet* rs = [db executeQuery:@"SELECT * FROM temples WHERE valid_address>=?", [NSNumber numberWithInteger:validAddress]];
        
        while([rs next])
        {
            Temple* temple = [self convertResultSetToTemple:rs];
            [temples addObject:temple];
        }
        
        [rs close];
    }];
    
    return temples;
}

- (NSArray*)getTemplesWithReligion:(NSString*)religion validAddress:(NSInteger)validAddress
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    __block NSMutableArray* temples = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        
        FMResultSet* rs = [db executeQuery:@"SELECT * FROM temples WHERE religion=? AND valid_address>=?", religion, [NSNumber numberWithInteger:validAddress]];
        
        while([rs next])
        {
            Temple* temple = [self convertResultSetToTemple:rs];
            [temples addObject:temple];
        }
        
        [rs close];
    }];
    
    return temples;
}

-(NSInteger)getTempleCountWithDistrict:(NSString*)district andReligion:(NSString*)religion andDeity:(NSString*)deity andKeyword:(NSString*)keyword
{
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return -1;
    }
    
    NSMutableString* whereCondition = [self makeWhereConditionWithDistrict:district andReligion:religion andDeity:deity andKeyword:keyword];
    
    __block NSInteger count = 0;
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        
        FMResultSet* rs;
        NSString* query;
        
        if(keyword)
        {
            NSString* kw = [NSString stringWithFormat:@"%%%@%%", keyword];
// TODO: Remove favorite<10 AND valid_address<20
#if FIX_DB_MODE
            query = [NSString stringWithFormat:@"SELECT COUNT(*) AS `count` FROM temples WHERE favorite<10 AND valid_address<20 AND valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#else
            query = [NSString stringWithFormat:@"SELECT COUNT(*) AS `count` FROM temples WHERE valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#endif
            rs = [db executeQuery:query, kw, kw];
        }
        else
        {
// TODO: Remove favorite<10 AND valid_address<20
#if FIX_DB_MODE
            query = [NSString stringWithFormat:@"SELECT COUNT(*) AS `count` FROM temples WHERE favorite<10 AND valid_address<20 AND valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#else
            query = [NSString stringWithFormat:@"SELECT COUNT(*) AS `count` FROM temples WHERE valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#endif
            rs = [db executeQuery:query];
        }
        
        [Utility debug:query];
        
        if([rs next])
        {
            count = [rs intForColumn:@"count"];
        }
        
        [rs close];
    }];
    
    return count;
}

-(NSArray*)getTempleWithDistrict:(NSString*)district andReligion:(NSString*)religion andDeity:(NSString*)deity andKeyword:(NSString*)keyword
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    NSMutableString* whereCondition = [self makeWhereConditionWithDistrict:district andReligion:religion andDeity:deity andKeyword:keyword];
    
    __block NSMutableArray* temples = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        
        FMResultSet* rs;
        NSString* query;
        
        if(keyword)
        {
            NSString* kw = [NSString stringWithFormat:@"%%%@%%", keyword];
// TODO: Remove favorite<10 AND valid_address<20
#if FIX_DB_MODE
            query = [NSString stringWithFormat:@"SELECT * FROM temples WHERE favorite<10 AND valid_address<20 AND valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#else
            query = [NSString stringWithFormat:@"SELECT * FROM temples WHERE valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#endif
            rs = [db executeQuery:query, kw, kw];
        }
        else
        {
// TODO: Remove favorite<10 AND valid_address<20
#if FIX_DB_MODE
            query = [NSString stringWithFormat:@"SELECT * FROM temples WHERE favorite<10 AND valid_address<20 AND valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#else
            query = [NSString stringWithFormat:@"SELECT * FROM temples WHERE valid_address>5 AND latitude>20 AND latitude<30 AND longitude>116 AND longitude<125 AND %@", whereCondition];
#endif
            rs = [db executeQuery:query];
        }
        
        [Utility debug:query];
        
        while([rs next])
        {
            Temple* temple = [self convertResultSetToTemple:rs];
            [temples addObject:temple];
        }
        
        [rs close];
    }];
    
    return temples;
}

- (NSMutableArray*)getTempleColumnGroupByColumn:(NSString*)columnName where:(NSString*)where orderBy:(NSString*)orderBy
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    __block NSMutableArray* result = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = [NSString stringWithFormat:@"SELECT %@,COUNT(*) AS `count` FROM temples %@ GROUP BY %@ %@", columnName, where, columnName, orderBy];
        FMResultSet* rs = [db executeQuery:query];
        
        [Utility debug:query];
        
        while([rs next])
        {
            NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
            
            NSString* r = [rs stringForColumn:columnName];
            int n = [rs intForColumn:@"count"];
            
            [data setValue:r forKey:columnName];
            [data setValue:[NSNumber numberWithInt:n] forKey:@"count"];
            [result addObject:data];
        }
    }];
    
    return result;
}

- (NSMutableArray*)getFavoriteTemples
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    __block NSMutableArray* result = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = [NSString stringWithFormat:@"SELECT * FROM temples WHERE favorite=%d", FAVORITE_LOVE];
        FMResultSet* rs = [db executeQuery:query];
        
        [Utility debug:query];
        
        while([rs next])
        {
            [result addObject:[self convertResultSetToTemple:rs]];
        }
    }];
    
    return result;
}

- (void)updateIntegerColumn:(NSString*)columnName withId:(NSUInteger)tid andValue:(NSInteger)newValue
{
    [Utility debug:@"%@ | column=%@ | tid=%d | value=%d", NSStringFromSelector(_cmd), columnName, tid, newValue];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return;
    }
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = [NSString stringWithFormat:@"UPDATE temples SET %@=%lu WHERE id=%lu", columnName, (long)newValue, (unsigned long)tid];
        [db executeUpdate:query];
    }];
}

- (void)updateRealColumn:(NSString*)columnName withId:(NSUInteger)tid andValue:(double)newValue
{
    [Utility debug:@"%@ | column=%@ | tid=%d | value=%f", NSStringFromSelector(_cmd), columnName, tid, newValue];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return;
    }
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = [NSString stringWithFormat:@"UPDATE temples SET %@=%f WHERE id=%lu", columnName, newValue, (unsigned long)tid];
        [db executeUpdate:query];
    }];
}

- (void)updateTextColumn:(NSString*)columnName withId:(NSUInteger)tid andValue:(NSString*)newValue
{
    [Utility debug:@"%@ | column=%@ | tid=%d | value=%@", NSStringFromSelector(_cmd), columnName, tid, newValue];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return;
    }
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = [NSString stringWithFormat:@"UPDATE temples SET %@=:newValue WHERE id=:tid", columnName];
        [db executeUpdate:query, newValue, [NSNumber numberWithUnsignedInteger:tid]];
    }];
}

- (void)updateFavoriteWithId:(NSUInteger)tid andValue:(NSInteger)newValue
{
    [self updateIntegerColumn:@"favorite" withId:tid andValue:newValue];
}

- (void)updateValidAddressWithId:(NSUInteger)tid andValue:(int)newValue
{
    [self updateIntegerColumn:@"valid_address" withId:tid andValue:newValue];
}

- (void)updateCoordinate:(NSUInteger)tid toLatitude:(double)latitude andLongitude:(double)longitude
{
    [self updateRealColumn:@"latitude" withId:tid andValue:latitude];
    [self updateRealColumn:@"longitude" withId:tid andValue:longitude];
}

- (Temple*)convertResultSetToTemple:(FMResultSet*)rs
{
    if(!rs)
        return nil;
    
    Temple* temple = [[Temple alloc] init];
    temple.tid = [rs intForColumn:@"id"];
    temple.seq = [rs stringForColumn:@"seq"];
    temple.chName = [rs stringForColumn:@"ch_name"];
    temple.enName = [rs stringForColumn:@"en_name"];
    temple.deity = [rs stringForColumn:@"deity"];
    temple.district = [rs stringForColumn:@"district"];
    temple.origAddress = [rs stringForColumn:@"orig_address"];
    temple.address = [rs stringForColumn:@"address"];
    temple.latitude = [rs doubleForColumn:@"latitude"];
    temple.longitude = [rs doubleForColumn:@"longitude"];
    temple.religion = [rs stringForColumn:@"religion"];
    temple.tel = [rs stringForColumn:@"tel"];
    temple.chargePerson = [rs stringForColumn:@"charge_person"];
    temple.validAddress = [rs intForColumn:@"valid_address"];
    temple.favorite = [rs intForColumn:@"favorite"];
    
    return temple;
}

#pragma mark - Temple structure

- (NSMutableArray*)getAllTempleStructures
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if (!_fmdbq)
    {
        [Utility warn:@"_fmdbq is nil"];
        @throw [[NSException alloc] initWithName:@"DATABASE_NO_OPEN" reason:@"_fmdbq is nil" userInfo:nil];
        return nil;
    }
    
    __block NSMutableArray* result = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = [NSString stringWithFormat:@"SELECT * FROM temple_structures"];
        FMResultSet* rs = [db executeQuery:query];
        
        [Utility debug:query];
        
        while([rs next])
        {
            [result addObject:[self convertResultSetToTempleStructure:rs]];
        }
    }];
    
    return result;
}

- (TempleStructure*)convertResultSetToTempleStructure:(FMResultSet*)rs
{
    if(!rs)
        return nil;
    
    TempleStructure* structure = [[TempleStructure alloc] init];
    structure.sid = [rs intForColumn:@"sid"];
    structure.chName = [rs stringForColumn:@"ch_name"];
    structure.html = [rs stringForColumn:@"html"];
    structure.url = [rs stringForColumn:@"url"];
    
    return structure;
}

#pragma mark - temp methods

- (void)patchTable
{
//    [_fmdbq inDatabase:^(FMDatabase *db) {
//        NSString* query = [NSString stringWithFormat:@"UPDATE temples SET valid_address=10 WHERE valid_address=1"];
//        [db executeQuery:query];
//    }];
//    
//    [_fmdbq inDatabase:^(FMDatabase *db) {
//        NSString* query = [NSString stringWithFormat:@"ALTER TABLE temples ADD favorite INTEGER DEFAULT 5"];
//        [db executeQuery:query];
//    }];
}

- (void)fixData
{
    NSMutableArray* temples = [[NSMutableArray alloc] init];
    
    [_fmdbq inDatabase:^(FMDatabase *db) {
        NSString* query = @"SELECT * FROM temples WHERE orig_address LIKE '%路%里%段%'";
        FMResultSet* rs = [db executeQuery:query];
        
        while([rs next])
        {
            Temple* t = [self convertResultSetToTemple:rs];
            [temples addObject:t];
        }
    }];
    
    for(Temple* t in temples)
    {
        NSString* address = t.origAddress;
        NSMutableString* result = [address mutableCopy];
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(.*路)(.*[里鄰村鄉鎮區])(.*段)" options:0 error:nil];
        
        [regex replaceMatchesInString:result options:0 range:NSMakeRange(0, [result length]) withTemplate:@"$1$3"];
        address = result;
        
        [self updateTextColumn:@"address" withId:t.tid andValue:address];
        
        __block double lat, lng;
        
        CLGeocoder* geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
            
            if([placemarks count] > 0)
            {
                // Get the first one
                CLPlacemark* mark = (CLPlacemark*)[placemarks objectAtIndex:0];
                lat = mark.location.coordinate.latitude;
                lng = mark.location.coordinate.longitude;
                
                [Utility debug:@"[%@] %@ %f %f", t.chName, address, lat, lng];
                [self updateCoordinate:t.tid toLatitude:lat andLongitude:lng];
            }
            else
            {
                [Utility warn:@"Geocoding fail: %@", [error localizedDescription]];
                lat = -1;
                lng = -1;
            }
        }];
    }
}

@end