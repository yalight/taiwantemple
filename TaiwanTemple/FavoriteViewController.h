//
//  FavoriteViewController.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/9.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempleSearchResultCell.h"
#import "MapView.h"

@interface FavoriteViewController : UITableViewController<UISearchBarDelegate, TempleCellEventDelegate, MapViewDelegate>

@end
