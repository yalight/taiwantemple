//
//  FavoriteViewController.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/9.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <objc/runtime.h>
#import "FavoriteViewController.h"
#import "MapView.h"
#import "Database.h"
#import "TempleSearchResultCell.h"

#define DISPLAY_MODE_LIST 0
#define DISPLAY_MODE_MAP  1

#define TEMPLE_SEARCH_RESULT_CELL @"TempleSearchResultCell"
#define MAP_CELL @"MapCell"

@interface FavoriteViewController ()

@property (strong, nonatomic) UISegmentedControl* modeSegmentControl;
@property (strong, nonatomic) UIBarButtonItem* editItem;

@property (strong, nonatomic) MapView* mapView;
@property (strong, nonatomic) MapView* searchMapView;

@property (strong, nonatomic) Database* database;
@property (strong, nonatomic) NSMutableArray* favoriteTemples;
@property (strong, nonatomic) NSArray* searchResultTemples;

@property (assign, nonatomic) NSInteger currentMode;
@property (assign, nonatomic) BOOL isInEdit;

@end

@implementation FavoriteViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initSettings];
    }
    return self;
}

- (void)initSettings
{
    _currentMode = DISPLAY_MODE_LIST;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    
    // Set search bar embedded into navigation bar
//    self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
//    CGRect frame = self.searchDisplayController.searchBar.frame;
//    frame.size.height = 0;
//    self.searchDisplayController.searchBar.frame = frame;
    
    // Add right bar item of navigation bar
    _modeSegmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"列表", @"List"), NSLocalizedString(@"地圖", @"Map")]];
    _modeSegmentControl.selectedSegmentIndex = _currentMode;
    [_modeSegmentControl addTarget:self action:@selector(changeDisplayMode) forControlEvents:UIControlEventValueChanged];
    
    _editItem = self.editButtonItem;
    [_editItem setTarget:self];
    [_editItem setAction:@selector(editButtonToggle)];
    UIBarButtonItem* modeItem = [[UIBarButtonItem alloc] initWithCustomView:_modeSegmentControl];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 66 + _modeSegmentControl.frame.size.width, 44)];
    toolBar.barStyle = -1;   //透明背景
    toolBar.clipsToBounds = YES;   //修正上方的線
    [toolBar setItems:@[modeItem, _editItem] animated:NO];
    
    UIBarButtonItem* rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:toolBar];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    // Setup map view
    CGRect barFrame = self.navigationController.navigationBar.frame;
    CGRect frame = CGRectMake(0, 0, barFrame.size.width, [Utility screenHeight] - barFrame.origin.y - barFrame.size.height - self.searchDisplayController.searchBar.frame.size.height + 1);
    _mapView = [Utility mapView1WithFrame:frame andDelegate:self andHasAd:HAS_AD andFullscreen:NO];
    frame.size.height += self.searchDisplayController.searchBar.frame.size.height;
    _searchMapView = [Utility mapView2WithFrame:frame andDelegate:self andHasAd:HAS_AD];
    [_mapView hideExpandAndCollapseButton];
    [_searchMapView hideExpandAndCollapseButton];
    
    // For fix table view seperator displaying problem
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.searchDisplayController.searchResultsTableView setSeparatorInset:UIEdgeInsetsZero];
    
    [self startWorkProcess];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)startWorkProcess
{
    _database = [Database sharedInstance];
    [self.tableView registerNib:[UINib nibWithNibName:@"TempleSearchResultCell" bundle:nil] forCellReuseIdentifier:TEMPLE_SEARCH_RESULT_CELL];
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"TempleSearchResultCell" bundle:nil] forCellReuseIdentifier:TEMPLE_SEARCH_RESULT_CELL];
    
    _favoriteTemples = [_database getFavoriteTemples];
    
    // Add temple markers to map
    for(Temple* temple in _favoriteTemples)
    {
        [_mapView addSpotWithTemple:temple];
    }
    
    [_mapView fitBounds];
}

- (void)changeDisplayMode
{
    static CGFloat previousContentOffset = 0;
    
    [Utility debug:@"%@:%d", NSStringFromSelector(_cmd), _currentMode];
    
    if(_currentMode == DISPLAY_MODE_MAP)
    {
        self.tableView.contentOffset = CGPointMake(0, previousContentOffset);
        _currentMode = DISPLAY_MODE_LIST;
        
        _editItem.enabled = YES;
    }
    else if(_currentMode == DISPLAY_MODE_LIST)
    {
        previousContentOffset = self.tableView.contentOffset.y;
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        _currentMode = DISPLAY_MODE_MAP;
        
        _editItem.enabled = NO;
        
        if(_isInEdit)
        {
            _modeSegmentControl.enabled = YES;
        }
    }
    
    [self.tableView reloadData];
    [self.searchDisplayController.searchResultsTableView reloadData];
}

#pragma mark - UITableView datasource datasource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount;
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        if(_currentMode == DISPLAY_MODE_LIST)
        {
            [self.searchDisplayController.searchResultsTableView setBounces:YES];
            rowCount = [_searchResultTemples count];
        }
        else if(_currentMode == DISPLAY_MODE_MAP)
        {
            [self.searchDisplayController.searchResultsTableView setBounces:NO];
            
            if([_searchResultTemples count] == 0)
                rowCount = 0;
            else
                rowCount = 1;
        }
    }
    else if(tableView == self.tableView)
    {
        if(_currentMode == DISPLAY_MODE_LIST)
        {
            [self.tableView setScrollEnabled:YES];
            [self.tableView setBounces:YES];
            rowCount = [_favoriteTemples count];
        }
        else if(_currentMode == DISPLAY_MODE_MAP)
        {
            [self.tableView setScrollEnabled:NO];
            [self.tableView setBounces:NO];
            rowCount = 1;
        }
    }
    
    [Utility debug:@"Row count = %d", rowCount];
    
    return rowCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    UITableViewCell* resultCell;
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        if(_currentMode == DISPLAY_MODE_LIST)
        {
            TempleSearchResultCell* cell = [tableView dequeueReusableCellWithIdentifier:TEMPLE_SEARCH_RESULT_CELL];
            
            NSInteger index = indexPath.row;
            Temple* temple = [_searchResultTemples objectAtIndex:index];
            
            cell.nameLabel.text = temple.chName;
            cell.religionLabel.text = temple.religion;
            cell.deityLabel.text = temple.deity;
            cell.addressLabel.text = temple.address;
            cell.data = temple;
            cell.delegate = self;
            
            if(_isInEdit)
            {
                cell.mapImage.hidden = YES;
                cell.starButton.hidden = NO;
                
                if(temple.favorite <= FAVORITE_NORMAL)
                    [cell enableStartButton:NO];
                else
                    [cell enableStartButton:YES];
            }
            else
            {
                cell.mapImage.hidden = NO;
                cell.starButton.hidden = YES;
            }
            
            resultCell = cell;
        }
        else if(_currentMode == DISPLAY_MODE_MAP)
        {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:MAP_CELL];
            
            if(!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MAP_CELL];
                [cell.contentView addSubview:_searchMapView];
            }
            
            resultCell = cell;
        }
    }
    else if(tableView == self.tableView)
    {
        if(_currentMode == DISPLAY_MODE_LIST)
        {
            TempleSearchResultCell* cell = [tableView dequeueReusableCellWithIdentifier:TEMPLE_SEARCH_RESULT_CELL];
            
            NSInteger index = indexPath.row;
            Temple* temple = [_favoriteTemples objectAtIndex:index];
            
            cell.nameLabel.text = temple.chName;
            cell.religionLabel.text = temple.religion;
            cell.deityLabel.text = temple.deity;
            cell.addressLabel.text = temple.address;
            cell.data = temple;
            cell.delegate = self;
            
            if(_isInEdit)
            {
                cell.mapImage.hidden = YES;
                cell.starButton.hidden = NO;
                [cell enableStartButton:YES];
                
                if(temple.favorite <= FAVORITE_NORMAL)
                    [cell enableStartButton:NO];
                else
                    [cell enableStartButton:YES];
            }
            else
            {
                cell.mapImage.hidden = NO;
                cell.starButton.hidden = YES;
            }
            
            resultCell = cell;
        }
        else if(_currentMode == DISPLAY_MODE_MAP)
        {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:MAP_CELL];
            
            if(!cell)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MAP_CELL];
                [cell.contentView addSubview:_mapView];
            }
            
            resultCell = cell;
        }
    }

    return resultCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 88;
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        if(_currentMode == DISPLAY_MODE_LIST)
        {
            TempleSearchResultCell* cell = [tableView dequeueReusableCellWithIdentifier:TEMPLE_SEARCH_RESULT_CELL];
            height = cell.frame.size.height;
        }
        else if(_currentMode == DISPLAY_MODE_MAP)
        {
            height = _searchMapView.frame.size.height;
        }
    }
    else if(tableView == self.tableView)
    {
        if(_currentMode == DISPLAY_MODE_LIST)
        {
            TempleSearchResultCell* cell = [tableView dequeueReusableCellWithIdentifier:TEMPLE_SEARCH_RESULT_CELL];
            height = cell.frame.size.height;
        }
        else if(_currentMode == DISPLAY_MODE_MAP)
        {
            height = _mapView.frame.size.height;
        }
    }
    
    return height;
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
//
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewCellEditingStyleDelete;
//}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Delete the row from the data source.
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//        [_favoriteTemples removeObjectAtIndex:indexPath.row];
//        
//        // Delete the row from the table view.
//        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        
//        // Reload the table view.
//        [tableView reloadData];
//    }
//}

#pragma mark - UITableView delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Utility debug:@"Selected at row %l", indexPath.row];
    
    if(_currentMode == DISPLAY_MODE_LIST)
    {
        if(tableView == self.tableView)
        {
            [_mapView switchMapMode:GMAP_MODE_MAP];
            [_mapView setSelectedMarkerWithIndex:indexPath.row andZoom:13];
            [self changeDisplayMode];
            _modeSegmentControl.selectedSegmentIndex = DISPLAY_MODE_MAP;
        }
        else if(tableView == self.searchDisplayController.searchResultsTableView)
        {
            [_searchMapView switchMapMode:GMAP_MODE_MAP];
            [_searchMapView setSelectedMarkerWithIndex:indexPath.row andZoom:13];
            [self changeDisplayMode];
            _modeSegmentControl.selectedSegmentIndex = DISPLAY_MODE_MAP;
        }
    }
}

#pragma mark - Cell event delegate methods

-(void)starButtonStateChanged:(TempleSearchResultCell*)cell andEnabled:(BOOL)enabled
{
    [Utility debug:@"cell star %@ : %@", cell.data.chName, enabled ? @"Enabled" : @"Disabled"];
    
    if(enabled)
        cell.data.favorite = FAVORITE_LOVE;
    else
        cell.data.favorite = FAVORITE_NORMAL;
}

#pragma mark - UIButton methods

- (void)editButtonToggle
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if(!_isInEdit) // Edit
    {
        _modeSegmentControl.enabled = NO;
        _editItem.title = NSLocalizedString(@"完成", @"Done");
        _editItem.style = UIBarButtonSystemItemDone;
        
        _isInEdit = YES;
        
        [self.tableView reloadData];
    }
    else // Done
    {
        _modeSegmentControl.enabled = YES;
        _editItem.title = NSLocalizedString(@"編輯", @"Edit");
        _editItem.style = UIBarButtonItemStylePlain;
        
        _isInEdit = NO;
        
        NSUInteger deleteCount = [self getDeleteCount];
        
        if(deleteCount > 0)
        {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"從收藏中移除", @"Remove from favorite")
                                                                message:[NSString stringWithFormat:NSLocalizedString(@"確定要從收藏中移除 %d 個廟宇？", @"Do you want to remove %d temples from favorite"), deleteCount]
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"取消", @"Cancel")
                                                      otherButtonTitles:NSLocalizedString(@"確定", @"OK"), nil];
            alertView.tag = 102;
            [alertView show];
        }
    }
}

- (NSUInteger)getDeleteCount
{
    NSUInteger count = 0;
    NSUInteger nTemple = [_favoriteTemples count];
    
    for(int i = 0; i<nTemple; i++)
    {
        Temple* temple = [_favoriteTemples objectAtIndex:i];
        
        if(temple.favorite == FAVORITE_NORMAL)
        {
            count++;
        }
    }

    return count;
}

- (void)deleteFavorites
{
    NSMutableIndexSet* deleteSet = [[NSMutableIndexSet alloc] init];
    
    NSUInteger nTemple = [_favoriteTemples count];
    
    for(NSUInteger i = 0; i<nTemple; i++)
    {
        Temple* temple = [_favoriteTemples objectAtIndex:i];
        
        if(temple.favorite == FAVORITE_NORMAL)
        {
            [deleteSet addIndex:i];
        }
    }
    
    // Update favorite value in DB
    [deleteSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        Temple* temple = [_favoriteTemples objectAtIndex:idx];
        // TODO: un-comment to really update favorite value in DB
        [_database updateFavoriteWithId:temple.tid andValue:FAVORITE_NORMAL];
    }];
    
    // remove from current _favoriteTemples
    [_favoriteTemples removeObjectsAtIndexes:deleteSet];
    
    // Update UI
    if([deleteSet count] > 0)
    {
        [_mapView hideCallout];
        [_mapView clearPath];
        [_mapView removeSpotWithIndexList:deleteSet];
        [_mapView clusterWithForceRerender:YES];
        [self.tableView reloadData];
    }
}

#pragma mark - Search methods

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [Utility debug:@"%@:%@", NSStringFromSelector(_cmd), searchText];
    
    NSPredicate* resultPredicate = [NSPredicate predicateWithFormat:@"SELF.chName contains[c] %@ or enName contains[c] %@", searchText, searchText];
    _searchResultTemples = [_favoriteTemples filteredArrayUsingPredicate:resultPredicate];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
//    [self updateData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(!self.searchDisplayController.searchBar.userInteractionEnabled)
        return;
    
    [Utility debug:@"%@:%@", NSStringFromSelector(_cmd), searchText];
    
    [_searchMapView switchMapMode:GMAP_MODE_MAP];
    
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // If there are too many data, showing HUD
    if([_searchResultTemples count] > 500)
    {
        [self updateDataWithHUD];
    }
    else
    {
        [self updateData];
    }
}

-(void)updateDataWithHUD
{
    [self.searchDisplayController.searchBar setUserInteractionEnabled:NO];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode= MBProgressHUDModeDeterminate;
    hud.dimBackground = YES;
    hud.square = YES;
    hud.labelText = NSLocalizedString(@"載入資料中", @"Loading");
    float initProgressDegree = 0.2;
    hud.progress = initProgressDegree;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [_searchMapView clearAllSpot];
            [_searchMapView clearPath];
        });
        
        NSUInteger nTemple = [_searchResultTemples count];
        NSUInteger batchSize = nTemple / 33;
        batchSize = (batchSize < 10 ? 10 : batchSize);
        
        for(int i=0; i<nTemple; i+=batchSize)
        {
            // batch add markers to map in main thread
            int endIndex = (i + batchSize > nTemple ? nTemple : i + batchSize);
            dispatch_sync(dispatch_get_main_queue(), ^{
                for(int j=i; j<endIndex; j++) {
                    Temple* t = [_searchResultTemples objectAtIndex:j];
                    [_searchMapView addSpotWithTemple:t];
                }
            });
            
            hud.progress = (initProgressDegree + (1 - initProgressDegree) * endIndex / nTemple);
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [_searchMapView fitBounds];
            
            UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            hud.customView = imageView;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(@"完成", @"Done");
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.searchDisplayController.searchBar setUserInteractionEnabled:YES];
        });
    });
}

-(void)updateData
{
    [_searchMapView clearAllSpot];
    [_searchMapView clearPath];
    
    for(Temple* temple in _searchResultTemples)
    {
        [_searchMapView addSpotWithTemple:temple];
    }
    
    [_searchMapView fitBounds];
}

#pragma mark - MapViewDelegate methods

- (UIViewController*)getViewController
{
    return self;
}

- (void)penButtonTap:(MapSpot*)spot
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"功能選擇", @"Select function")
                                                        message:NSLocalizedString(@"移除：移除此廟宇地標。\r更新：更新為目前坐標。", @"Remove: remove the temple marker.\rUpdate: update the temple marker coordinate.")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"取消", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"移除", @"Remove"), NSLocalizedString(@"更新", @"Update"), nil];
    alertView.tag = 100;
    objc_setAssociatedObject(alertView, @"map-spot", spot, OBJC_ASSOCIATION_RETAIN);
    [alertView show];
}

- (void)starButtonTap:(MapSpot*)spot enabled:(BOOL)enabled
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if(!enabled)
    {
        Temple* temple = spot.marker.userData;
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"從收藏中移除", @"Remove from favorite")
                                                            message:[NSString stringWithFormat:NSLocalizedString(@"確定要從收藏中移除「%@」？", @"Do you want to remove「%@」 from favorite"), temple.chName]
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"取消", @"Cancel")
                                                  otherButtonTitles:NSLocalizedString(@"確定", @"OK"), nil];
        alertView.tag = 101;
        objc_setAssociatedObject(alertView, @"map-spot", spot, OBJC_ASSOCIATION_RETAIN);
        [alertView show];
    }
}

-(void)infoButtonTap:(MapSpot *)spot
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
}

#pragma mark - AlertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100)
    {
        MapSpot* spot = objc_getAssociatedObject(alertView, @"map-spot");
        
        if(spot)
        {
            if(buttonIndex == 1) // 移除
            {
//                [self reportErrorTemple:spot];
            }
            else if(buttonIndex == 2) // 更新
            {
//                [self updateCoordinate:spot];
            }
        }
    }
    else if(alertView.tag == 101)
    {
        MapSpot* spot = objc_getAssociatedObject(alertView, @"map-spot");
        Temple* temple = spot.marker.userData;
        
        if(buttonIndex == 1)
        {
            MapSpot* targetSpot = [_mapView findSpotByUserData:temple];
            
            [_mapView clearPath];
            [_mapView hideCallout];
            [_mapView removeSpot:targetSpot];
            [_mapView clusterWithForceRerender:YES];
            
            // If user tab star in search mode, also need to remove spot in _searchResultMap
            if(_currentMode == DISPLAY_MODE_MAP && self.searchDisplayController.active)
            {
                targetSpot = [_searchMapView findSpotByUserData:temple];
                [_searchMapView clearPath];
                [_searchMapView hideCallout];
                [_searchMapView removeSpot:targetSpot];
                [_searchMapView clusterWithForceRerender:YES];
            }
            
            [_favoriteTemples removeObject:temple];
            [self.tableView reloadData];
            [self.searchDisplayController.searchResultsTableView reloadData];
            
            // TODO: un-comment to really update favorite value in DB
            [_database updateFavoriteWithId:temple.tid andValue:FAVORITE_NORMAL];
            
            [_mapView switchMapMode:GMAP_MODE_MAP];
            [_searchMapView switchMapMode:GMAP_MODE_MAP];
        }
        else if(buttonIndex == 0)
        {
            [_mapView enableStarButton:YES];
            [_searchMapView enableStarButton:YES];
        }
    }
    else if(alertView.tag ==  102)
    {
        if(buttonIndex == 1)
        {
            [self deleteFavorites];
        }
        else if(buttonIndex == 0)
        {
            // Recover the favorite value
            NSUInteger nTemple = [_favoriteTemples count];
            
            for(int i=0; i<nTemple; i++)
            {
                Temple* temple = [_favoriteTemples objectAtIndex:i];
                
                if(temple.favorite == FAVORITE_NORMAL)
                {
                    temple.favorite = FAVORITE_LOVE;
                }
            }
            
            [self.tableView reloadData];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
