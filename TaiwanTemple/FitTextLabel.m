//
//  FitTextLabel.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/24.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "FitTextLabel.h"

@implementation FitTextLabel

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setText:(NSString *)text
{
    UIFont* font = self.font;
    CGSize textSize = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    
    while(textSize.width > self.frame.size.width)
    {
        font = [UIFont fontWithName:font.fontName size:font.pointSize-1];
        textSize = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    }
    
    [self setFont:font];
    [super setText:text];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
