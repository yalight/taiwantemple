//
//  ViewController.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/1.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "HomeViewController.h"
#import "Database.h"

#define BUILD_DB 0

#if BUILD_DB
#import <GoogleMaps/GoogleMaps.h>
#endif

@interface HomeViewController ()

//@property (strong, nonatomic) IBOutlet UIScrollView *bannerScrollView;
//@property (strong, nonatomic) IBOutlet UIPageControl *bannerPageControl;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;

@property (strong, nonatomic) Database* database;
@property (strong, nonatomic) NSXMLParser* xmlParser;
@property (strong, nonatomic) Temple* currentTemple;

@property (assign, nonatomic) NSInteger fieldNumber;
@property (strong, nonatomic) NSString* tagName;
@property (strong, nonatomic) NSString* language;

@property (retain, nonatomic) dispatch_group_t dbInsertGroup;

@property (assign, nonatomic) BOOL hasAd;

@end

@implementation HomeViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hasAd = HAS_AD;
    
    if(_hasAd)
    {
        [[Utility sharedInstance] setupAdModBanner:_bannerView andRootViewController:self];
    }
    
    // setup navigation bar
    self.title = NSLocalizedString(@"台灣廟宇", @"Taiwan Temple");
    self.navigationController.navigationBarHidden = NO;
    
    // 設定 navigation bar back button 的文字
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"首頁", @"Home")
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:nil action:nil];
    
    [self initApp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)initApp
{
    if(!_database)
        _database = [Database sharedInstance];
    
#if BUILD_DB
    [_database removeDatabase];
    [_database createDatabase];
    
    _dbInsertGroup = dispatch_group_create();
    
    dispatch_group_async(_dbInsertGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self startParseData];
    });
    
    dispatch_group_notify(_dbInsertGroup, dispatch_get_main_queue(), ^{
        [Utility debug:@"==> ALL INSERT DONE !!!!"];
    });
#else
    [_database openDatabase];
#endif
    
    // Initialize religions colors and icons
    [Temple religionColors];
    [Temple religionMarkerIcons];
    
//    [_database fixData];
}

#if 0
- (void)setupBanner
{
    //UIScrollView 設定
    [_bannerScrollView setPagingEnabled:YES];
    [_bannerScrollView setShowsHorizontalScrollIndicator:NO];
    [_bannerScrollView setShowsVerticalScrollIndicator:NO];
    [_bannerScrollView setScrollsToTop:YES];
    [_bannerScrollView setDelegate:self];
    
    NSUInteger n =_bannerPageControl.numberOfPages;
    
    CGFloat width, height;
    width = _bannerScrollView.frame.size.width;
    height = _bannerScrollView.frame.size.height;
    [_bannerScrollView setContentSize:CGSizeMake(width * n, height)];
    
    //製作 ScrollView 的內容
    for (int i = 0; i < n; i++)
    {
        CGRect frame = CGRectMake(width*i, 0, width, height);
        
        UIImage* bannerImage = [UIImage imageNamed:[NSString stringWithFormat:@"banner%d.jpg", (i+1)]];
        
        if(bannerImage)
        {
            UIImageView* bannerImageView = [[UIImageView alloc] initWithFrame:frame];
            [bannerImageView setImage:bannerImage];
            [_bannerScrollView addSubview:bannerImageView];
        }
        else
        {
            UIView *view = [[UIView alloc] initWithFrame:frame];
            
            CGFloat r, g ,b;
            r = (arc4random() % 10) / 10.0;
            g = (arc4random() % 10) / 10.0;
            b = (arc4random() % 10) / 10.0;
            [view setBackgroundColor:[UIColor colorWithRed:r green:g blue:b alpha:0.3]];
            
            [_bannerScrollView addSubview:view];
        }
    }
}
#endif 

#if BUILD_DB
#pragma mark - XML parse methods

- (NSString*)correctAddress:(NSString*)address
{
    NSMutableString* result = [address mutableCopy];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(.*[路街段])(.*[村里鄰])*" options:0 error:nil];
    
    [regex replaceMatchesInString:result options:0 range:NSMakeRange(0, [result length]) withTemplate:@"$1"];
    
    return result;
}

-(void)startParseData
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"寺廟" ofType:@"rdf"];
    //NSString* path = [[NSBundle mainBundle] pathForResource:@"TempleSample" ofType:@"rdf"]; // small data for test
    NSData* data = [NSData dataWithContentsOfFile:path];
    NSUInteger nData = data.length;
    
    [Utility info:@"Data length = %d byte", nData];
    
    _xmlParser = [[NSXMLParser alloc] initWithData:data];
    [_xmlParser setDelegate:self];
    [_xmlParser parse];
    
    [Utility debug:@"End parse"];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    _tagName = elementName;
    
    
    if([@"moi:name" isEqualToString:elementName])
    {
        _language = [attributeDict objectForKey:@"xml:lang"];
    }
    else if([@"rdfs:label" isEqualToString:elementName])
    {
        _language = [attributeDict objectForKey:@"xml:lang"];
    }
    else if([@"moi:Temple" isEqualToString:elementName])
    {
        _fieldNumber = 1;
        _currentTemple = [[Temple alloc] init];
    }
    else if([@"moi:lordDeity" isEqualToString:elementName])
    {
        _fieldNumber = 2;
    }
    else if([@"moi:district" isEqualToString:elementName])
    {
        _fieldNumber = 3;
    }
    else if([@"moi:address" isEqualToString:elementName])
    {
        _fieldNumber = 4;
    }
    else if([@"moi:religion" isEqualToString:elementName])
    {
        _fieldNumber = 5;
    }
    else if([@"moi:tel" isEqualToString:elementName])
    {
        _fieldNumber = 6;
    }
    else if([@"moi:personInCharge" isEqualToString:elementName])
    {
        _fieldNumber = 7;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(_fieldNumber == 1)
    {
        if([_tagName isEqualToString:@"moi:name"])
        {
            if([_language isEqualToString:@"zh"])
            {
                if(!_currentTemple.chName)
                    _currentTemple.chName = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
            else if([_language isEqualToString:@"en"])
            {
                if(!_currentTemple.enName)
                    _currentTemple.enName = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
        }
        else if([_tagName isEqualToString:@"moi:id"])
        {
            if(!_currentTemple.seq)
                _currentTemple.seq = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
    }
    else if(_fieldNumber == 2)
    {
        if([_tagName isEqualToString:@"moi:name"])
        {
            if([_language isEqualToString:@"zh"])
            {
                if(!_currentTemple.deity)
                    _currentTemple.deity = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
        }
    }
    else if(_fieldNumber == 3)
    {
        if([_tagName isEqualToString:@"rdfs:label"])
        {
            if([_language isEqualToString:@"zh"])
            {
                if(!_currentTemple.district)
                    _currentTemple.district = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
        }
    }
    else if(_fieldNumber == 4)
    {
        if([_tagName isEqualToString:@"rdfs:label"])
        {
            if([_language isEqualToString:@"zh"])
            {
                if(!_currentTemple.origAddress)
                    _currentTemple.origAddress = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
        }
    }
    else if(_fieldNumber == 5)
    {
        if([_tagName isEqualToString:@"moi:name"])
        {
            if([_language isEqualToString:@"zh"])
            {
                if(!_currentTemple.religion)
                    _currentTemple.religion = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
        }
    }
    else if(_fieldNumber == 6)
    {
        if([_tagName isEqualToString:@"moi:tel"])
        {
            if(!_currentTemple.tel)
                _currentTemple.tel = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
    }
    else if(_fieldNumber == 7)
    {
        if([_tagName isEqualToString:@"moi:name"])
        {
            if(!_currentTemple.chargePerson)
                _currentTemple.chargePerson = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    CLGeocoder* geocoder;
    
    if(!geocoder)
        geocoder = [[CLGeocoder alloc] init];
    
    if([@"moi:Temple" isEqualToString:elementName])
    {
        _currentTemple.address = [self correctAddress:_currentTemple.origAddress];
        BOOL isValidAddress = [Temple isValidAddress:_currentTemple.origAddress];
        
        if(isValidAddress)
        {
            _currentTemple.validAddress = 1;
        }
        else
        {
            _currentTemple.validAddress = 0;
            [Utility info:@"(Invalid Address) ==> %@, %@", _currentTemple.seq, _currentTemple.address];
        }
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        [geocoder geocodeAddressString:_currentTemple.address completionHandler:^(NSArray *placemarks, NSError *error) {
            
            if([placemarks count] > 0)
            {
                // Get the first one
                CLPlacemark* mark = (CLPlacemark*)[placemarks objectAtIndex:0];
                double lat = mark.location.coordinate.latitude;
                double lng = mark.location.coordinate.longitude;
                
                _currentTemple.latitude = lat;
                _currentTemple.longitude = lng;
                
                [_database insertTempleRecord:_currentTemple];
            }
            else
            {
                [Utility warn:@"Geocoding fail: %@", [error localizedDescription]];
                _currentTemple.latitude = -1;
                _currentTemple.longitude = -1;
                [_database insertTempleRecord:_currentTemple];
            }
            
            dispatch_semaphore_signal(sema);
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
}
#endif

@end
