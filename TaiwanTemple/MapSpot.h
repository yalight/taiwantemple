//
//  MapSpot.h
//  Google Maps iOS Example
//
//  Created by Yalight on 2/1/14.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
@import CoreLocation;
#import "GClusterItem.h"

@interface MapSpot : NSObject <GClusterItem>

@property (nonatomic) CLLocationCoordinate2D location;
@property (strong, nonatomic) GMSMarker* marker;

@end
