//
//  MapSpot.m
//  Google Maps iOS Example
//
//  Created by Yalight on 2/1/14.
//

#import "MapSpot.h"

@implementation MapSpot

- (CLLocationCoordinate2D)position {
    return self.location;
}

- (BOOL)isEqualTo:(id<GClusterItem>)item
{
    if(item.position.latitude==self.position.latitude && item.position.longitude==self.position.longitude)
        return YES;
    
    return NO;
}

@end
