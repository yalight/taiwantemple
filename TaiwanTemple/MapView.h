//
//  MapView.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/24.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GoogleMaps/GMSMarker.h>
#import "MapSpot.h"
#import "SMCalloutView.h"
#import "Temple.h"

#define GMAP_MODE_MAP    1
#define GMAP_MODE_STREET 2

@protocol MapViewDelegate
@required
- (UIViewController*)getViewController;
@optional
- (void)penButtonTap:(MapSpot*)spot;
- (void)starButtonTap:(MapSpot*)spot enabled:(BOOL)enabled;
- (void)infoButtonTap:(MapSpot*)spot;
@end

@interface MapView : UIView <GMSMapViewDelegate, GMSPanoramaViewDelegate, SMCalloutViewDelegate>

@property (weak, nonatomic) id<MapViewDelegate> delegate;
@property (strong, nonatomic) UIView *googleMapView;
@property (strong, nonatomic) GMSMapView* mapView;
@property (strong, nonatomic) GMSPanoramaView* streetView;
@property (strong, nonatomic) UIImage* starShineImage;
@property (strong, nonatomic) UIImage* starEmptyImage;
@property (assign, nonatomic) BOOL isFullScreen;
@property (assign, nonatomic) BOOL hasAd;

+ (UIImage*)favoriteMarkerIcon;

- (void)addSpotWithTemple:(Temple*)temple;
- (void)removeSpot:(MapSpot*)spot;
- (void)removeSpotWithIndexList:(NSIndexSet*)deleteSet;
- (MapSpot*)getSpotAtIndex:(NSUInteger)index;
- (MapSpot*)findSpotByUserData:(id)data;
- (void)clearAllSpot;
- (void)switchMapMode:(NSInteger)mode;
- (void)fitBounds;
- (void)clearPath;
- (void)hideCallout;
- (void)tapMarkAtIndex:(NSUInteger)markerIndex withZoom:(float)zoom;
- (void)setSelectedMarkerWithIndex:(NSUInteger)index andZoom:(float)zoom;
- (void)centerToMarkerAtIndex:(NSUInteger)index andZoom:(float)zoom andShowInfoWindow:(BOOL)showInfoWindow;
- (void)showMarkerInfoWindowAtIndex:(NSUInteger)index;
- (CLLocationCoordinate2D)getStreetViewCoordinate;
- (void)clusterWithForceRerender:(BOOL)forceRerender;
- (void)hideExpandAndCollapseButton;
- (void)enableStarButton:(BOOL)enabled;

@end
