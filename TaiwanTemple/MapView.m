//
//  MapView.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/24.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "MapView.h"
#import "Utility.h"
#import "GClusterManager.h"
#import "NonHierarchicalDistanceBasedAlgorithm.h"
#import "GDefaultClusterRenderer.h"
#import "MapSpot.h"
#import "MDDirectionService.h"

static const CGFloat CalloutYOffset = 36.0f;
static const CGFloat CalloutFavoriteYOffset = 26.0f;
static const CGFloat TopButtonYOffset = 60;

@interface MapView ()

@property (strong, nonatomic) SMCalloutView *calloutView;
@property (strong, nonatomic) GClusterManager* clusterManager;
@property (strong, nonatomic) GMSCameraPosition* previousCameraPosition;

@property (strong, nonatomic) UIButton* zoomInButton;
@property (strong, nonatomic) UIButton* zoomOutButton;
@property (strong, nonatomic) UIButton* expandButton;
@property (strong, nonatomic) UIButton* collapseButton;

@property (strong, nonatomic) UIButton* starButton;
@property (strong, nonatomic) UIButton* penButton;
@property (strong, nonatomic) UIButton* infoButton;
@property (strong, nonatomic) UIButton* backButton;

@property (strong, nonatomic) NSMutableArray* markerPool;
@property (assign, nonatomic) NSInteger usedSpotCount;

@property (assign, nonatomic) NSInteger currentMode;
@property (strong, nonatomic) MapSpot* selectedMarker;

@property (assign, nonatomic) BOOL firstLocationUpdate;
@property (assign, nonatomic) CLLocationCoordinate2D myLocation;
@property (strong, nonatomic) GMSMarker* myLocationMarker;
@property (strong, nonatomic) GMSPolyline* previousPolyline;
@property (assign, nonatomic) CLLocationDistance routeLength;

@property (assign, nonatomic) CGRect collapseFrame;
@property (assign, nonatomic) CGRect expandFrame;

@property (strong, nonatomic) GADBannerView* bannerView;

@end

@implementation MapView

+ (UIImage*)favoriteMarkerIcon
{
    static UIImage* icon;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        icon = [Utility imageWithImage:[UIImage imageNamed:@"star-shine-icon"] scaledToSize:CGSizeMake(30, 30)];
    });
    
    return icon;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupMap];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setupMap];
    }
    return self;
}

- (void)dealloc {
    [_mapView removeObserver:self forKeyPath:@"myLocation" context:NULL];
}

- (void)setHasAd:(BOOL)hasAd
{
    if(hasAd)
    {
        if(!_bannerView)
        {
            CGRect frame = _googleMapView.bounds;
            UIViewController* vc = [_delegate getViewController];
            _bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, frame.size.height - 50)];
            [[Utility sharedInstance] setupAdModBanner:_bannerView andRootViewController:vc];
            [_googleMapView addSubview:_bannerView];
        }
        
        if(_currentMode == GMAP_MODE_MAP)
        {
            _bannerView.hidden = YES;
        }
        else
        {
            _bannerView.hidden = NO;
        }
    }
    else
    {
        if(_bannerView)
            [_bannerView removeFromSuperview];
    }
    
    _hasAd = hasAd;
}

- (void)setFrame:(CGRect)frame
{
    [Utility debug:@"%@%@", NSStringFromSelector(_cmd), NSStringFromCGRect(frame)];
    
    [super setFrame:frame];
    
    // change the map frame while the map frame changed
    [_googleMapView setFrame: self.bounds];
    [_mapView setFrame:self.bounds];
    
    if(_streetView)
    {
        CGRect streetViewFrame = _googleMapView.bounds;
        
        if(_hasAd)
        {
            streetViewFrame.size.height -= 50;
            CGRect bannerFrame = _bannerView.frame;
            bannerFrame.origin.y =_googleMapView.bounds.size.height - 50;
            _bannerView.frame = bannerFrame;
        }
        
        [_streetView setFrame:streetViewFrame];
    }
    
    [self resetButtonPosition];
}

- (void)setupMap
{
    _currentMode = GMAP_MODE_MAP;
    _usedSpotCount = 0;
    _markerPool = [[NSMutableArray alloc] init];
    _previousCameraPosition = nil;
    _selectedMarker = nil;
    _isFullScreen = NO;
    
    _collapseFrame = self.frame;
    _expandFrame = CGRectMake(0, 20, [Utility screenWidth], [Utility screenHeight] - 20);
    
    _starShineImage = [Utility imageWithImage:[UIImage imageNamed:@"star-shine-icon"] scaledToSize:CGSizeMake(25, 25)];
    _starEmptyImage = [Utility imageWithImage:[UIImage imageNamed:@"star-outline-icon"] scaledToSize:CGSizeMake(25, 25)];
    
    CGRect frame = self.bounds;
    _googleMapView = [[UIView alloc] initWithFrame:frame];
    _googleMapView.backgroundColor = [UIColor blackColor];
    [self addSubview:_googleMapView];
    
    
    _myLocation = CLLocationCoordinate2DMake(25.0167601,121.5403086);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_myLocation.latitude
                                                            longitude:_myLocation.longitude
                                                                 zoom:8];
    
    _mapView = [GMSMapView mapWithFrame:frame camera:camera];
    _mapView.delegate = self;
    _mapView.settings.compassButton = YES;
    //[_mapView setMinZoom:1 maxZoom:12];
    //_mapView.settings.myLocationButton = YES;
    
    // !!!: Begin Cluster Manager
    _clusterManager = [GClusterManager managerWithMapView:_mapView
                                                algorithm:[[NonHierarchicalDistanceBasedAlgorithm alloc] initWithMaxDistanceAtZoom:32]
                                                 renderer:[[GDefaultClusterRenderer alloc] initWithMapView:_mapView]];
    // !!!: End Cluster Manager
    
    // Add my location marker
//    _myLocationMarker = [[GMSMarker alloc] init];
//    _myLocationMarker.position = _myLocation;
//    _myLocationMarker.icon = [Utility imageWithImage:[UIImage imageNamed:@"star-shine-icon"] scaledToSize:CGSizeMake(24, 24)];
//    _myLocationMarker.map = _mapView;
    
    // Listen to the myLocation property of GMSMapView.
    [_mapView addObserver:self
              forKeyPath:@"myLocation"
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
    
    [_googleMapView addSubview:_mapView];
    [self addFunctionButtons];
}

- (void)addFunctionButtons
{
    UIImage* zoomInImage = [Utility imageWithImage:[UIImage imageNamed:@"zoom-in-icon"] scaledToSize:CGSizeMake(25, 25)];
    _zoomInButton = [[UIButton alloc] init];
    [_zoomInButton setImage:zoomInImage forState:UIControlStateNormal];
    _zoomInButton.backgroundColor = [UIColor colorWithHue:0.602 saturation:0.762 brightness:0.880 alpha:0.30];
    _zoomInButton.layer.cornerRadius = 15;
    CGRect frame = CGRectMake(_mapView.bounds.size.width - 52, _mapView.bounds.size.height - 57, 42, 42);
    _zoomInButton.frame = frame;
    [_zoomInButton addTarget:self action:@selector(zoomInMap) forControlEvents:UIControlEventTouchUpInside];
    [_mapView addSubview:_zoomInButton];
    
    UIImage* zoomOutImage = [Utility imageWithImage:[UIImage imageNamed:@"zoom-out-icon"] scaledToSize:CGSizeMake(25, 25)];
    _zoomOutButton = [[UIButton alloc] init];
    [_zoomOutButton setImage:zoomOutImage forState:UIControlStateNormal];
    _zoomOutButton.backgroundColor = [UIColor colorWithHue:0.602 saturation:0.762 brightness:0.880 alpha:0.30];
    _zoomOutButton.layer.cornerRadius = 15;
    frame = CGRectMake(_mapView.bounds.size.width - 52, _mapView.bounds.size.height - 105, 42, 42);
    _zoomOutButton.frame = frame;
    [_zoomOutButton addTarget:self action:@selector(zoomOutMap) forControlEvents:UIControlEventTouchUpInside];
    [_mapView addSubview:_zoomOutButton];
    
    UIImage* expandImage = [Utility imageWithImage:[UIImage imageNamed:@"up-icon"] scaledToSize:CGSizeMake(25, 25)];
    _expandButton = [[UIButton alloc] init];
    [_expandButton setImage:expandImage forState:UIControlStateNormal];
    _expandButton.backgroundColor = [UIColor colorWithHue:1.000 saturation:0.619 brightness:0.959 alpha:0.680];
    _expandButton.layer.cornerRadius = 15;
    frame = CGRectMake(_googleMapView.bounds.size.width - 52, 10, 42, 42);
    _expandButton.frame = frame;
    [_expandButton addTarget:self action:@selector(expandButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [_googleMapView addSubview:_expandButton];
    
    UIImage* collapseImage = [Utility imageWithImage:[UIImage imageNamed:@"down-icon"] scaledToSize:CGSizeMake(25, 25)];
    _collapseButton = [[UIButton alloc] init];
    [_collapseButton setImage:collapseImage forState:UIControlStateNormal];
    _collapseButton.backgroundColor = [UIColor colorWithHue:0.495 saturation:0.636 brightness:0.862 alpha:0.680];
    _collapseButton.layer.cornerRadius = 15;
    frame = CGRectMake(_googleMapView.bounds.size.width - 52, 10, 42, 42);
    _collapseButton.frame = frame;
    _collapseButton.hidden = YES;
    [_collapseButton addTarget:self action:@selector(collapseButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [_googleMapView addSubview:_collapseButton];
}

- (void)clusterWithForceRerender:(BOOL)forceRerender
{
    [_clusterManager clusterWithForceRerender:forceRerender];
}

#pragma mark - public methods

- (void)addSpotWithTemple:(Temple*)temple
{
//    [Utility debug:@"addMarkWithTemple => %@ (%lf, %lf)", temple.chName, temple.latitude, temple.longitude];
    
    MapSpot* spot;
    
    if(_usedSpotCount < [_markerPool count])
    {
        spot = [_markerPool objectAtIndex:_usedSpotCount];
    }
    else
    {
        spot = [[MapSpot alloc] init];
        spot.marker = [[GMSMarker alloc] init];
        [_markerPool addObject:spot];
    }
    
//    spot.marker.map = _mapView; // trick: need to add marker to map first and than move it!
    spot.location = CLLocationCoordinate2DMake(temple.latitude, temple.longitude);
    spot.marker.userData = temple;
    
    if(temple.favorite == FAVORITE_LOVE)
    {
        spot.marker.icon = [MapView favoriteMarkerIcon];
    }
    else
    {
        spot.marker.icon = [Temple religionMarkerIcon:temple.religion];
    }
    
    spot.marker.position = CLLocationCoordinate2DMake(temple.latitude, temple.longitude);

    _usedSpotCount ++;
    [_clusterManager addItem:spot];
}

- (void)removeSpot:(MapSpot*)spot
{
    [_clusterManager removeItem:spot];
}

- (void)removeSpotWithIndexList:(NSIndexSet*)deleteSet
{
    [deleteSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSUInteger spotIndex = idx;
        [self removeSpot:[_markerPool objectAtIndex:spotIndex]];
        [_markerPool removeObjectAtIndex:spotIndex];
    }];
}

- (MapSpot*)getSpotAtIndex:(NSUInteger)index
{
    if(index < _usedSpotCount)
        return [_markerPool objectAtIndex:index];
    else
        return nil;
}

- (MapSpot*)findSpotByUserData:(id)data
{
    for(int i=0; i<_usedSpotCount; i++)
    {
        MapSpot* spot = [_markerPool objectAtIndex:i];
        if(spot.marker.userData == data)
            return spot;
    }
    
    return nil;
}

- (void)clearAllSpot
{
    if(_calloutView)
    {
        _calloutView.hidden = YES;
    }

    _mapView.selectedMarker = nil;
    _selectedMarker = nil;
    
    NSUInteger nSpot = [_markerPool count];
    
    for(int i=0; i<_usedSpotCount && i<nSpot; i++)
    {
        MapSpot* spot = [_markerPool objectAtIndex:i];
        spot.marker.map = nil;
    }
    
    [_clusterManager removeItems];
    
    _usedSpotCount = 0;
}

- (void)fitBounds
{
    GMSCameraPosition* previousCameraPosition = _mapView.camera;
    
    GMSCoordinateBounds *bounds;
    
    NSUInteger nSpot = [_markerPool count];
    
    for(int i=0; i<_usedSpotCount && i<nSpot; i++)
    {
        MapSpot* spot = [_markerPool objectAtIndex:i];
        
        if (bounds == nil)
        {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:spot.marker.position
                                                          coordinate:spot.marker.position];
        }
        
        bounds = [bounds includingCoordinate:spot.marker.position];
    }
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:36.0f];
    [_mapView moveCamera:update];
    
    // Reset the camera zoom value to avoid too large zoom value
    if(_mapView.camera.zoom >= 13)
    {
        GMSCameraPosition* camera = _mapView.camera;
        GMSCameraPosition* newCamera = [GMSCameraPosition cameraWithLatitude:camera.target.latitude longitude:camera.target.longitude zoom:12];
        [_mapView setCamera:newCamera];
    }
    
    // If zoom doesn't changed, need to force to rereder the map markers
    if(previousCameraPosition.zoom == _mapView.camera.zoom)
    {
        [_clusterManager clusterWithForceRerender:YES];
    }
}

- (void)clearPath
{
    if(_previousPolyline)
    {
        _previousPolyline.map = nil;
    }
}

- (void)hideCallout
{
    if(_calloutView)
    {
        [_calloutView setHidden:YES];
    }
}

- (CLLocationCoordinate2D)getStreetViewCoordinate
{
    if(!_streetView)
    {
        // return an invalid coordinate to be an error
        return CLLocationCoordinate2DMake(100, 200);
    }
    
    return _streetView.panorama.coordinate;
}

- (void)tapMarkAtIndex:(NSUInteger)markerIndex withZoom:(float)zoom
{
    if(markerIndex >= [_markerPool count])
        return;
    
    MapSpot* spot = [_markerPool objectAtIndex:markerIndex];
    GMSMarker* marker = spot.marker;
    
    [self mapView:_mapView didTapMarker:marker];
    
    // Center to marker
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.22f];  // 0.22 second animation
    [_mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:marker.position.latitude
                                                                  longitude:marker.position.longitude
                                                                       zoom:zoom]];
    [CATransaction commit];
}

- (void)setSelectedMarkerWithIndex:(NSUInteger)index andZoom:(float)zoom
{
    if(index >= _usedSpotCount)
        return;
    
    _selectedMarker = [_markerPool objectAtIndex:index];
    [self centerToMarkerAtIndex:index andZoom:zoom andShowInfoWindow:YES];
    [self showMarkerInfoWindowAtIndex:index];
}

- (void)centerToMarkerAtIndex:(NSUInteger)index andZoom:(float)zoom andShowInfoWindow:(BOOL)showInfoWindow
{
    if(index >= _usedSpotCount) // invalid index
        return;
    
    MapSpot* spot = [_markerPool objectAtIndex:index];
    GMSMarker* marker = spot.marker;
    GMSCameraUpdate* update = [GMSCameraUpdate setTarget:marker.position zoom:13];
    [_mapView moveCamera:update];
    
    if(showInfoWindow)
    {
        [self showMarkerInfoWindowAtIndex:index];
    }
}

- (void)showMarkerInfoWindowAtIndex:(NSUInteger)index
{
    if(index >= _usedSpotCount) // invalid index
    {
        return;
    }
    
    if(!_calloutView)
    {
        _calloutView = [self createMarkerCallout];
    }
    
    MapSpot* spot = [_markerPool objectAtIndex:index];
    GMSMarker* marker = spot.marker;
    
    Temple* temple = (Temple*) marker.userData;
    NSMutableString* templeName = [temple.chName mutableCopy];
    
    // Remove 財團法人OO縣 from temple name to avoid too long name shown on UI
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(財團法人.*[縣市])(.*)" options:0 error:nil];
    [regex replaceMatchesInString:templeName options:0 range:NSMakeRange(0, [templeName length]) withTemplate:@"$2"];
    
    _calloutView.title = templeName;
    
    if(temple.favorite == FAVORITE_LOVE)
    {
        _calloutView.calloutOffset = CGPointMake(0, -CalloutFavoriteYOffset);
    }
    else
    {
        _calloutView.calloutOffset = CGPointMake(0, -CalloutYOffset);
    }
    
    _calloutView.hidden = NO;
    
    CLLocationCoordinate2D anchor = marker.position;
    CGPoint point = [_mapView.projection pointForCoordinate:anchor];
    
    CGRect calloutRect = CGRectZero;
    calloutRect.origin = point;
    calloutRect.size = CGSizeZero;
    
    [_calloutView presentCalloutFromRect:calloutRect
                                  inView:_mapView
                       constrainedToView:_mapView
                                animated:YES];
}

- (void)hideExpandAndCollapseButton
{
    _expandButton.hidden = YES;
    _collapseButton.hidden = YES;
}

- (void)enableStarButton:(BOOL)enabled
{
    if(enabled)
    {
        [_starButton setImage:_starShineImage forState:UIControlStateNormal];
    }
    else
    {
        [_starButton setImage:_starEmptyImage forState:UIControlStateNormal];
    }
}

#pragma mark - private methods

- (SMCalloutView*)createMarkerCallout
{
    SMCalloutView* calloutView = [SMCalloutView platformCalloutView];
    calloutView.delegate = self;
    
    // create a little accessory view to mimic the little car that Maps.app shows
    UIImageView *carView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Driving"]];
    
    if([calloutView.backgroundView isKindOfClass:[SMCalloutMaskedBackgroundView class]])
    {
        // wrap it in a blue background on iOS 7+
        UIButton *blueView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44+30)];
        blueView.backgroundColor = [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1];
        [blueView addTarget:self action:@selector(carClicked) forControlEvents:UIControlEventTouchUpInside];
        
        carView.frame = CGRectMake(11, 14, carView.image.size.width, carView.image.size.height);
        [blueView addSubview:carView];
        
        calloutView.leftAccessoryView = blueView;
        
        // i按鈕, create a little disclosure indicator since our callout is tappable
        //        UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //        [disclosure addTarget:self action:@selector(disclosureTapped) forControlEvents:UIControlEventTouchUpInside];
        //        calloutView.rightAccessoryView = disclosure;
        
        // 箭頭按鈕
        calloutView.rightAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableNext"]];
        calloutView.rightAccessoryView.alpha = 0.2;
    }
    else
    {
        // "inset" the car graphic to match the callout's title on iOS 6-
        carView.layer.shadowOffset = CGSizeMake(0, -1);
        carView.layer.shadowColor = [UIColor colorWithWhite:0 alpha:0.5].CGColor;
        carView.layer.shadowOpacity = 1;
        carView.layer.shadowRadius = 0;
        carView.clipsToBounds = NO;
        calloutView.leftAccessoryView = carView;
    }
    
    return calloutView;
}

- (void)createStreetView
{
    CGRect frame = _googleMapView.bounds;
    
    if(_hasAd)
    {
        frame.size.height -= 50;
        
        UIViewController* vc = [_delegate getViewController];
        
        if(!_bannerView)
        {
            _bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, frame.size.height)];
            [[Utility sharedInstance] setupAdModBanner:_bannerView andRootViewController:vc];
            [_googleMapView addSubview: _bannerView];
        }
    }
    
    _streetView = [GMSPanoramaView panoramaWithFrame:frame nearCoordinate:_selectedMarker.position];
    _streetView.backgroundColor = [UIColor grayColor];
    _streetView.delegate = self;
    [_googleMapView addSubview:_streetView];
    
    // Create street view function buttons
    //UIImage* backArrowImage = [UIImage imageNamed:@"UITableNext"];
    //backArrowImage = [UIImage imageWithCGImage:backArrowImage.CGImage scale:backArrowImage.scale orientation:UIImageOrientationUpMirrored];
    
    UIImage* backArrowImage = [Utility imageWithImage:[UIImage imageNamed:@"left-icon"] scaledToSize:CGSizeMake(22, 22)];
    _backButton = [[UIButton alloc] init];
    [_backButton setImage:backArrowImage forState:UIControlStateNormal];
    _backButton.backgroundColor = [UIColor colorWithHue:0.495 saturation:0.636 brightness:0.862 alpha:0.680];
    _backButton.layer.cornerRadius = 15;
    frame = CGRectMake(10, (_isFullScreen? TopButtonYOffset : 10), 42, 42);
    _backButton.frame = frame;
    [_backButton addTarget:self action:@selector(backToMap) forControlEvents:UIControlEventTouchUpInside];
    [_streetView addSubview:_backButton];

#if FIX_DB_MODE
    _penButton = [[UIButton alloc] init];
    UIImage* penImage = [Utility imageWithImage:[UIImage imageNamed:@"editor-icon"] scaledToSize:CGSizeMake(25, 25)];
    [_penButton setImage:penImage forState:UIControlStateNormal];
    _penButton.backgroundColor = [UIColor colorWithHue:0.992 saturation:0.589 brightness:0.882 alpha:0.680];
    _penButton.layer.cornerRadius = 15;
    frame = CGRectMake(_streetView.bounds.size.width - 52, _streetView.bounds.size.height - 156, 42, 42);
    _penButton.frame = frame;
    [_penButton addTarget:self action:@selector(penButtonTapUpInside) forControlEvents:UIControlEventTouchUpInside];
    [_streetView addSubview:_penButton];
#endif
    
    _infoButton = [[UIButton alloc] init];
    UIImage* infoImage = [Utility imageWithImage:[UIImage imageNamed:@"info-icon"] scaledToSize:CGSizeMake(25, 25)];
    [_infoButton setImage:infoImage forState:UIControlStateNormal];
    _infoButton.backgroundColor = [UIColor colorWithHue:0.201 saturation:0.930 brightness:0.700 alpha:0.430];//[UIColor colorWithHue:0.992 saturation:0.589 brightness:0.882 alpha:0.680];
    _infoButton.layer.cornerRadius = 15;
    frame = CGRectMake(_streetView.bounds.size.width - 52, _streetView.bounds.size.height - 108, 42, 42);
    _infoButton.frame = frame;
    [_infoButton addTarget:self action:@selector(infoButtonTapUpInside) forControlEvents:UIControlEventTouchUpInside];
    [_streetView addSubview:_infoButton];
    
    _starButton = [[UIButton alloc] init];
    _starButton.backgroundColor = [UIColor colorWithHue:0.091 saturation:0.873 brightness:0.953 alpha:0.450];//[UIColor colorWithHue:0.992 saturation:0.589 brightness:0.882 alpha:0.680];
    _starButton.layer.cornerRadius = 15;
    frame = CGRectMake(_streetView.bounds.size.width - 52, _streetView.bounds.size.height - 60, 42, 42);
    _starButton.frame = frame;
    [_starButton addTarget:self action:@selector(starButtonTapUpInside) forControlEvents:UIControlEventTouchUpInside];
    [_streetView addSubview:_starButton];
    [self setupButtonWithTemple:_selectedMarker.marker.userData];
    
    [_googleMapView bringSubviewToFront:_expandButton];
    [_googleMapView bringSubviewToFront:_collapseButton];
}

- (void)switchMapMode:(NSInteger)mode
{
    [Utility debug:NSStringFromSelector(_cmd)];
    
    if(mode == _currentMode)
        return;
    
    if(!_streetView)
    {
        [self createStreetView];
    }
    
    if(mode == GMAP_MODE_STREET)
    {
        if(_hasAd)
        {
            GADRequest *request = [GADRequest request];
            [_bannerView loadRequest:request];
            _bannerView.hidden = NO;
        }
        
        [_streetView moveNearCoordinate:_selectedMarker.position];
        _mapView.hidden = YES;
        _streetView.hidden = NO;
        
        _currentMode = mode;
    }
    else if(mode == GMAP_MODE_MAP)
    {
        if(_bannerView)
            _bannerView.hidden = YES;
        
        _streetView.hidden = YES;
        _mapView.hidden = NO;
        
        _currentMode = mode;
    }
    else
    {
        [Utility debug:@"== Undefined Map Mode =="];
        @throw [[NSException alloc] initWithName:@"Undefined Map Mode" reason:[NSString stringWithFormat:@"Cannot switch to undefined mode %lu", (long)mode] userInfo:nil];
    }
}

- (void)showSelectedMarkerInfo
{
    if(!_selectedMarker)
        return;
    
    Temple* temple = _selectedMarker.marker.userData;
    
    NSString* message = [NSString stringWithFormat:@"宗教: %@\r供奉神祉: %@\r地址: %@\r距離: %.0f 公尺 = %.2f 公里\r聯絡人: %@\r電話: %@",
                         temple.religion, temple.deity, temple.address, _routeLength, _routeLength / 1000, temple.chargePerson, temple.tel];
    
    NSString* secondButtonTitle;
    
    if(_currentMode == GMAP_MODE_MAP)
    {
        secondButtonTitle = @"看街景";
    }
    else
    {
        secondButtonTitle = @"回地圖";
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:temple.chName
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"看完了"
                                              otherButtonTitles:secondButtonTitle, nil];
    alertView.tag = 168;
    
    [alertView show];
}

- (void)setupButtonWithTemple:(Temple*)temple
{
    if(!temple)
        return;
    
    if(_starButton)
    {
        NSString* favoriteImageName = @"star-outline-icon";
        
        if(temple.favorite > FAVORITE_NORMAL) {
            favoriteImageName = @"star-shine-icon";
        }
        
        UIImage* favoriteImage = [Utility imageWithImage:[UIImage imageNamed:favoriteImageName] scaledToSize:CGSizeMake(24, 24)];
        [_starButton setImage:favoriteImage forState:UIControlStateNormal];
    }
}

- (void)resetButtonPosition
{
    CGRect frame;
    
    if(_isFullScreen)
    {
        frame = _collapseButton.frame;
        frame.origin.y = TopButtonYOffset;
        _collapseButton.frame= frame;
        _expandButton.frame = frame;
        _collapseButton.hidden = NO;
        _expandButton.hidden = YES;
        
        if(_backButton)
        {
            frame = _backButton.frame;
            frame.origin.y = TopButtonYOffset;
            _backButton.frame = frame;
        }
    }
    else
    {
        frame = _collapseButton.frame;
        frame.origin.y = 10;
        _collapseButton.frame= frame;
        _expandButton.frame = frame;
        _collapseButton.hidden = YES;
        _expandButton.hidden = NO;
        
        if(_backButton)
        {
            frame = _backButton.frame;
            frame.origin.y = 10;
            _backButton.frame = frame;
        }
    }
    
    frame = _zoomInButton.frame;
    frame.origin.y = self.frame.size.height - 105;
    _zoomInButton.frame = frame;
    
    frame = _zoomOutButton.frame;
    frame.origin.y = self.frame.size.height - 57;
    _zoomOutButton.frame = frame;
    
    if(_starButton)
    {
        frame = _starButton.frame;
        frame.origin.y = _streetView.frame.size.height - 60;
        _starButton.frame = frame;
    }
    
    if(_infoButton)
    {
        frame = _infoButton.frame;
        frame.origin.y = _streetView.frame.size.height - 108;
        _infoButton.frame = frame;
    }
    
    if(_penButton)
    {
        frame = _penButton.frame;
        frame.origin.y = _streetView.frame.size.height - 156;
        _penButton.frame = frame;
    }
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (!_firstLocationUpdate)
    {
        // If the first location update has not yet been recieved, then jump to that location.
        _firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:12];
        _myLocation = location.coordinate;
        _myLocationMarker.position = _myLocation;
        
        [Utility debug:@"My Location: (%f, %f)", _myLocation.latitude, _myLocation.longitude];
    }
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    assert(mapView == _mapView);
    
    // Don't re-compute clusters if the map has just been panned/tilted/rotated.
    GMSCameraPosition *position = [mapView camera];
    if (_previousCameraPosition != nil && _previousCameraPosition.zoom == position.zoom) {
        return;
    }
    _previousCameraPosition = [mapView camera];
    
    [_clusterManager clusterWithForceRerender:NO];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    [Utility debug:@"Tap marker coordinate = (%f, %f)", marker.position.latitude, marker.position.longitude];
    
    // Center to marker
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.22f];  // 0.22 second animation
    [mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:marker.position.latitude
                                                                 longitude:marker.position.longitude
                                                                      zoom:mapView.camera.zoom]];
    [CATransaction commit];
    
    // Cluster marker
    if(!marker.userData) {
        return YES;
    }
    
    _selectedMarker = [[MapSpot alloc] init];
    _selectedMarker.location = marker.position;
    _selectedMarker.marker = marker;
    mapView.selectedMarker = marker;
    
    Temple* temple = marker.userData;
    [self createRouteFrom:_myLocation to:CLLocationCoordinate2DMake(temple.latitude, temple.longitude)];
    [self setupButtonWithTemple:temple];
    
    return NO;
}

- (UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker*)marker
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    if(!_calloutView)
    {
        _calloutView = [self createMarkerCallout];
    }
    
    Temple* temple = (Temple*) marker.userData;
    NSMutableString* templeName = [temple.chName mutableCopy];
    
    // Remove 財團法人OO縣 from temple name to avoid too long name shown on UI
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(財團法人.*[縣市])(.*)" options:0 error:nil];
    [regex replaceMatchesInString:templeName options:0 range:NSMakeRange(0, [templeName length]) withTemplate:@"$2"];
    
    _calloutView.title = templeName;
    
    if(temple.favorite == FAVORITE_LOVE)
    {
        _calloutView.calloutOffset = CGPointMake(0, -CalloutFavoriteYOffset);
    }
    else
    {
        _calloutView.calloutOffset = CGPointMake(0, -CalloutYOffset);
    }
    
    _calloutView.hidden = NO;
    
    CLLocationCoordinate2D anchor = marker.position;
    CGPoint point = [mapView.projection pointForCoordinate:anchor];
    
    CGRect calloutRect = CGRectZero;
    calloutRect.origin = point;
    calloutRect.size = CGSizeZero;
    
    [_calloutView presentCalloutFromRect:calloutRect
                                  inView:mapView
                       constrainedToView:mapView
                                animated:YES];
    
    return nil;
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    /* move callout with map drag */
    if (_selectedMarker != nil && !self.calloutView.hidden)
    {
        CLLocationCoordinate2D anchor = [_selectedMarker position];
        
        CGPoint arrowPt = _calloutView.backgroundView.arrowPoint;
        CGPoint pt = [mapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        
        Temple* temple = (Temple*)_selectedMarker.marker.userData;
        
        if(temple.favorite == FAVORITE_LOVE)
        {
            pt.y -= arrowPt.y + CalloutFavoriteYOffset;
        }
        else
        {
            pt.y -= arrowPt.y + CalloutYOffset;
        }
        
        _calloutView.frame = (CGRect) {.origin = pt, .size = self.calloutView.frame.size};
    }
    else
    {
        _calloutView.hidden = YES;
    }
    
    [_clusterManager clusterWithForceRerender:NO];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    _selectedMarker = nil;
    _calloutView.hidden = YES;
}

#pragma mark - GMSPanoramaDelegate

- (void)panoramaView:(GMSPanoramaView *)panoramaView didMoveCamera:(GMSPanoramaCamera *)camera {
    NSLog(@"Camera: (%f,%f,%f)", camera.orientation.heading, camera.orientation.pitch, camera.zoom);
}

- (void)panoramaView:(GMSPanoramaView *)view didMoveToPanorama:(GMSPanorama *)panorama
{
    static GMSMarker* previousMarker;
    
    if(previousMarker)
    {
        previousMarker.panoramaView = nil;
        previousMarker = _selectedMarker.marker;
    }
    
    if(_selectedMarker)
    {
        _selectedMarker.marker.panoramaView = _streetView;
    }
}

- (BOOL)panoramaView:(GMSPanoramaView *)panoramaView didTapMarker:(GMSMarker *)marker
{
    [self showSelectedMarkerInfo];
    return YES;
}

#pragma mark - SMCallout methods

- (void)calloutAccessoryButtonTapped:(id)sender
{
}

- (void)carClicked
{
    [self switchMapMode:GMAP_MODE_STREET];
}

- (void)calloutViewClicked:(SMCalloutView *)calloutView
{
    [self showSelectedMarkerInfo];
}

#pragma mark - AlertView methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [Utility debug:@"AlertView tag:%d buttonIndex:%d", alertView.tag, buttonIndex];
    
    if(alertView.tag == 168) // 廟宇資訊
    {
        if(buttonIndex == 1)
        {
            switch (_currentMode) {
                case GMAP_MODE_MAP:
                    [self switchMapMode:GMAP_MODE_STREET];
                    break;
                    
                case GMAP_MODE_STREET:
                    [self switchMapMode:GMAP_MODE_MAP];
                    break;
                    
                default:
                    [Utility debug:@"Unhandled map mode switching"];
                    break;
            }
        }
    }
}

#pragma mark - Button event callback methods

- (void)zoomInMap
{
    [_mapView animateToZoom:_mapView.camera.zoom + 1];
}

- (void)zoomOutMap
{
    [_mapView animateToZoom:_mapView.camera.zoom - 1];
}

- (void)backToMap
{
    [self switchMapMode:GMAP_MODE_MAP];
}

- (void)penButtonTapUpInside
{
    [_delegate penButtonTap:_selectedMarker];
}

- (void)starButtonTapUpInside
{
    if(_selectedMarker.marker.userData)
    {
        BOOL enabled;
        Temple* temple = _selectedMarker.marker.userData;
        
        if(temple.favorite <= FAVORITE_NORMAL)
        {
            UIImage* starImage = _starShineImage;
            [_starButton setImage:starImage forState:UIControlStateNormal];
            enabled = YES;
        }
        else
        {
            UIImage* starImage = _starEmptyImage;
            [_starButton setImage:starImage forState:UIControlStateNormal];
            enabled = NO;
        }
        
        [_delegate starButtonTap:_selectedMarker enabled:enabled];
    }
}

- (void)infoButtonTapUpInside
{
    [self showSelectedMarkerInfo];
    [_delegate infoButtonTap:_selectedMarker];
}

- (void)expandButtonTap
{
    _expandButton.hidden = YES;
    _collapseButton.hidden = NO;
    self.frame = _expandFrame;
    _isFullScreen = YES;
    [self resetButtonPosition];
}

- (void)collapseButtonTap
{
    _expandButton.hidden = NO;
    _collapseButton.hidden = YES;
    self.frame = _collapseFrame;
    _isFullScreen = NO;
    [self resetButtonPosition];
}

#pragma mark - MDDirectionService methods

- (void)createRouteFrom:(CLLocationCoordinate2D)coordinate1 to:(CLLocationCoordinate2D)coordinate2
{
    NSMutableArray* waypoints_ = [[NSMutableArray alloc]init];
    NSMutableArray* waypointStrings_ = [[NSMutableArray alloc]init];
    
    // from coordinate
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(coordinate1.latitude,
                                                                 coordinate1.longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    [waypoints_ addObject:marker];
    
    NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                coordinate1.latitude,coordinate1.longitude];
    
    [waypointStrings_ addObject:positionString];
    
    // to coordinate
    position = CLLocationCoordinate2DMake(coordinate2.latitude,
                                          coordinate2.longitude);
    marker = [GMSMarker markerWithPosition:position];
    [waypoints_ addObject:marker];
    
    positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                      coordinate2.latitude,coordinate2.longitude];
    
    [waypointStrings_ addObject:positionString];
    
    if([waypoints_ count]>1)
    {
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        
        MDDirectionService *mds=[[MDDirectionService alloc] init];
        
        SEL selector = @selector(addDirections:);
        
        [mds setDirectionsQuery:query
                   withSelector:selector
                   withDelegate:self];
    }
}

- (void)addDirections:(NSDictionary *)json
{
    if(_previousPolyline) // Remove previous route
    {
        _previousPolyline.map = nil;
    }
    
    NSDictionary *routes = [json objectForKey:@"routes"][0];
    NSDictionary *route = [routes objectForKey:@"overview_polyline"];
    NSString *overview_route = [route objectForKey:@"points"];
    GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeWidth = 6;
    polyline.strokeColor = [UIColor colorWithHue:0.552 saturation:0.911 brightness:0.907 alpha:0.75];
    polyline.map = _mapView;
    
    _previousPolyline = polyline;

    _routeLength = [path lengthOfKind:kGMSLengthGeodesic];
    [Utility debug:@"Path length = %f meters", _routeLength];
}

@end
