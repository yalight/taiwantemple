//
//  MapViewController.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/1.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import <GoogleMaps/GMSMarker.h>
#import "SMCalloutView.h"

#import "MapViewController.h"
#import "Database.h"
#import "Temple.h"
#import "Utility.h"

#define MODE_MAP    1
#define MODE_STREET 2

static const CGFloat CalloutYOffset = 36.0f;

@interface MapViewController ()

@property (strong, nonatomic) IBOutlet UITextField *templeIdField;
@property (strong, nonatomic) IBOutlet UIView *googleMapView;
@property (strong, nonatomic) UIBarButtonItem *rightBarButton;

@property (strong, nonatomic) GMSMapView* mapView;
@property (strong, nonatomic) GMSPanoramaView* streetView;

@property (strong, nonatomic) SMCalloutView *calloutView;

@property (strong, nonatomic) Database* database;

@property (assign, nonatomic) BOOL firstLocationUpdate;

@property (assign, nonatomic) GMSMarker* selectedMarker;

@property (assign, nonatomic) NSInteger currentMode;

@end

@implementation MapViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"地圖", @"Map");
    self.navigationController.navigationBarHidden = NO;

    //adding the button to the UINavigationBar right side
//    _rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"街景模式" style:UIBarButtonItemStylePlain target:self action:@selector(changeMapMode)];
    _rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:nil];
    self.navigationItem.rightBarButtonItem = _rightBarButton;
    
    [self startMainProcess];
}

- (void)dealloc {
    [_mapView removeObserver:self forKeyPath:@"myLocation" context:NULL];
}

- (void)startMainProcess
{
    [self setupMap];
 
    if(!_database)
        _database = [Database sharedInstance];
    
    NSArray* temples = [_database getTemplesWithReligion:@"佛教" validAddress:1];
    
    for(int i=0; i<60 && i<[temples count]; i++)
    {
        Temple* t = [temples objectAtIndex:i];
        [Utility debug:[t toString]];
        [self addMarkerWithTemple:t];
    }
}

- (void)setupMap
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:25.03
                                                            longitude:121.30
                                                                 zoom:8];
    
    CGRect frame = _googleMapView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    _mapView = [GMSMapView mapWithFrame:frame camera:camera];
    _mapView.settings.compassButton = YES;
    //_mapView.settings.myLocationButton = YES;
    _mapView.delegate = self;
    
    // Listen to the myLocation property of GMSMapView.
    [_mapView addObserver:self
              forKeyPath:@"myLocation"
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    _currentMode = MODE_MAP;
    [_googleMapView addSubview:_mapView];
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
}

- (void)addMarkerWithTemple:(Temple*)temple
{
    [Utility debug:@"addMarkWithTemple => %@ (%lf, %lf)", temple.chName, temple.latitude, temple.longitude];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.userData = temple;
    marker.icon = [GMSMarker markerImageWithColor:[Temple religionColor:temple.religion]];
    marker.position = CLLocationCoordinate2DMake(temple.latitude, temple.longitude);
    marker.map = _mapView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)printButtonTap:(id)sender
{
    NSInteger tid = [_templeIdField.text integerValue];
    Temple* t = [_database getTempleWithId:tid];
    [Utility info:@"==> %@", [t toString]];
}

#pragma mark - private methods

- (SMCalloutView*)createMarkerCallout
{
    SMCalloutView* calloutView = [SMCalloutView platformCalloutView];
    calloutView.delegate = self;
    
    // create a little accessory view to mimic the little car that Maps.app shows
    UIImageView *carView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Driving"]];
    
    if([calloutView.backgroundView isKindOfClass:[SMCalloutMaskedBackgroundView class]])
    {
        // wrap it in a blue background on iOS 7+
        UIButton *blueView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44+30)];
        blueView.backgroundColor = [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1];
        [blueView addTarget:self action:@selector(carClicked) forControlEvents:UIControlEventTouchUpInside];
        
        carView.frame = CGRectMake(11, 14, carView.image.size.width, carView.image.size.height);
        [blueView addSubview:carView];
        
        calloutView.leftAccessoryView = blueView;
        
        // i按鈕, create a little disclosure indicator since our callout is tappable
//        UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        [disclosure addTarget:self action:@selector(disclosureTapped) forControlEvents:UIControlEventTouchUpInside];
//        calloutView.rightAccessoryView = disclosure;
        
        // 箭頭按鈕
        calloutView.rightAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UITableNext"]];
        calloutView.rightAccessoryView.alpha = 0.2;
    }
    else
    {
        // "inset" the car graphic to match the callout's title on iOS 6-
        carView.layer.shadowOffset = CGSizeMake(0, -1);
        carView.layer.shadowColor = [UIColor colorWithWhite:0 alpha:0.5].CGColor;
        carView.layer.shadowOpacity = 1;
        carView.layer.shadowRadius = 0;
        carView.clipsToBounds = NO;
        calloutView.leftAccessoryView = carView;
    }
    
    return calloutView;
}

- (void)showSelectedMarkerInfo
{
    if(!_selectedMarker)
        return;
    
    Temple* temple = _selectedMarker.userData;
    
    NSString* message = [NSString stringWithFormat:@"宗教: %@\r供奉神祉: %@\r地址: %@\r聯絡人: %@\r電話: %@",
                         temple.religion, temple.deity, temple.address, temple.chargePerson, temple.tel];
    
    NSString* secondButtonTitle;
    
    if(_currentMode == MODE_MAP)
    {
        secondButtonTitle = @"看街景";
    }
    else
    {
        secondButtonTitle = @"回地圖";
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:temple.chName
                                                        message:message
                                                       delegate:self
                                                        cancelButtonTitle:@"看完了"
                                                        otherButtonTitles:secondButtonTitle, nil];
    alertView.tag = 168;
    
    [alertView show];
}

#pragma mark - Button callback methods

- (IBAction)streetModeButtonTap:(id)sender
{
    [self changeMapMode];
}

- (void)changeMapMode
{
    [Utility debug:NSStringFromSelector(_cmd)];
    
    if(!_streetView)
    {
        CGRect frame = _googleMapView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        
        _streetView = [GMSPanoramaView panoramaWithFrame:frame nearCoordinate:_selectedMarker.position];
        _streetView.backgroundColor = [UIColor grayColor];
        _streetView.delegate = self;
    }
    else
    {
        [_streetView moveNearCoordinate:_selectedMarker.position];
    }
    
    if(_currentMode == MODE_MAP)
    {
        _currentMode = MODE_STREET;
        [_mapView removeFromSuperview];
        [_googleMapView addSubview:_streetView];
    }
    else
    {
        _currentMode = MODE_MAP;
        [_streetView removeFromSuperview];
        [_googleMapView addSubview:_mapView];
    }
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (!_firstLocationUpdate)
    {
        // If the first location update has not yet been recieved, then jump to that location.
        _firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:14];
    }
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    [Utility debug:@"Tap marker coordinate = (%f, %f)", marker.position.latitude, marker.position.longitude];
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.22f];  // 0.22 second animation
    [mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:marker.position.latitude
                                                        longitude:marker.position.longitude
                                                        zoom:mapView.camera.zoom]];
    [CATransaction commit];
    
    _selectedMarker = marker;
    mapView.selectedMarker = marker;
    
    return NO;
}

- (UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker*)marker
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    CLLocationCoordinate2D anchor = marker.position;
    
    CGPoint point = [mapView.projection pointForCoordinate:anchor];
    
    if(!_calloutView)
    {
        _calloutView = [self createMarkerCallout];
    }
    
    Temple* temple = (Temple*) marker.userData;
    _calloutView.title = temple.chName;
    
    _calloutView.calloutOffset = CGPointMake(0, -CalloutYOffset);
    
    _calloutView.hidden = NO;
    
    CGRect calloutRect = CGRectZero;
    calloutRect.origin = point;
    calloutRect.size = CGSizeZero;
    
    [_calloutView presentCalloutFromRect:calloutRect
                  inView:mapView
                  constrainedToView:mapView
                  animated:YES];
    
    return nil;
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    /* move callout with map drag */
    if (_selectedMarker != nil && !self.calloutView.hidden)
    {
        CLLocationCoordinate2D anchor = [_selectedMarker position];
        
        CGPoint arrowPt = _calloutView.backgroundView.arrowPoint;
        CGPoint pt = [mapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        pt.y -= arrowPt.y + CalloutYOffset;
        
        _calloutView.frame = (CGRect) {.origin = pt, .size = self.calloutView.frame.size};
    }
    else
    {
        _calloutView.hidden = YES;
    }
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    _selectedMarker = nil;
    _calloutView.hidden = YES;
}

#pragma mark - GMSPanoramaDelegate

- (void)panoramaView:(GMSPanoramaView *)panoramaView didMoveCamera:(GMSPanoramaCamera *)camera {
    NSLog(@"Camera: (%f,%f,%f)", camera.orientation.heading, camera.orientation.pitch, camera.zoom);
}

- (void)panoramaView:(GMSPanoramaView *)view didMoveToPanorama:(GMSPanorama *)panorama
{
    static GMSMarker* previousMarker;
    
    if(previousMarker)
    {
        previousMarker.panoramaView = nil;
        previousMarker = _selectedMarker;
    }
    
    if(_selectedMarker)
    {
        _selectedMarker.panoramaView = _streetView;
    }
}

- (BOOL)panoramaView:(GMSPanoramaView *)panoramaView didTapMarker:(GMSMarker *)marker
{
    [self showSelectedMarkerInfo];
    return YES;
}

#pragma mark - SMCallout methods

- (void)calloutAccessoryButtonTapped:(id)sender
{
    if (_mapView.selectedMarker)
    {
        GMSMarker *marker = _mapView.selectedMarker;
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:marker.title
                                                      message:marker.snippet
                                                      delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)carClicked
{
    [self changeMapMode];
}

- (void)calloutViewClicked:(SMCalloutView *)calloutView
{
    [self showSelectedMarkerInfo];
}

#pragma mark - AlertView methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [Utility debug:@"AlertView tag:%d buttonIndex:%d", alertView.tag, buttonIndex];
    
    if(alertView.tag == 168) // 廟宇資訊
    {
        if(buttonIndex == 1)
        {
            [self changeMapMode];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
