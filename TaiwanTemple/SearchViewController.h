//
//  SearchViewController.h
//  TaiwanTemple
//
//  Created by yalight on 2014/9/20.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"

@interface SearchViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, MapViewDelegate>

@end
