//
//  SearchViewController.m
//  TaiwanTemple
//
//  Created by yalight on 2014/9/20.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <objc/runtime.h>

#import "SearchViewController.h"
#import "Utility.h"
#import "Database.h"
#import "TempleSearchResultCell.h"
#import "MapView.h"

#define FIELD_DISTRICT 1
#define FIELD_RELIGION 2
#define FIELD_DEITY 3

#define TEMPLE_SEARCH_RESULT_CELL @"TempleSearchResultCell"

@interface SearchViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *districtButton;
@property (strong, nonatomic) IBOutlet UIButton *religionButton;
@property (strong, nonatomic) IBOutlet UIButton *deityButton;
@property (strong, nonatomic) IBOutlet UITextField *keywordTextField;
@property (strong, nonatomic) IBOutlet UIView *searchResultCountView;
@property (strong, nonatomic) IBOutlet UIButton *searchResultCountLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *modeSegmentControl;
@property (strong, nonatomic) IBOutlet UITableView *searchResultTableView;
@property (strong, nonatomic) IBOutlet UIView *pickerMaskView;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIView *searchResultView;
@property (strong, nonatomic) IBOutlet UIView *guideView;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
@property (strong, nonatomic) GADBannerView *guideBannerView;
@property (strong, nonatomic) MapView *searchResultMap;

@property (strong, nonatomic) Database* database;
@property (strong, nonatomic) NSMutableArray* districts;
@property (strong, nonatomic) NSMutableArray* religions;
@property (strong, nonatomic) NSMutableArray* deities;
@property (assign, nonatomic) NSInteger currentPickerField;
@property (assign, nonatomic) NSInteger selectedDistrict;
@property (assign, nonatomic) NSInteger selectedReligion;
@property (assign, nonatomic) NSInteger selectedDeity;

@property (assign, nonatomic) NSInteger searchResultCount;
@property (strong, nonatomic) NSArray* searchResultTemples;

@property (assign, nonatomic) CGRect pickerShowFrame;
@property (assign, nonatomic) CGRect pickerHideFrame;

@property (assign, nonatomic) BOOL hasAd;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hasAd = HAS_AD;
    
    if(_hasAd)
    {
        [[Utility sharedInstance] setupAdModBanner:_bannerView andRootViewController:self];
        
//        _guideBannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner origin:CGPointMake(0, _guideView.frame.size.height - 100)];
//        [[Utility sharedInstance] setupAdModBanner:_guideBannerView andRootViewController:self];
//        [_guideView addSubview:_guideBannerView];
    }
    
    self.title = NSLocalizedString(@"搜尋廟宇", @"Search Temple");
    
    // For fix table view seperator displaying problem
    [_searchResultTableView setSeparatorInset:UIEdgeInsetsZero];
    [_searchResultTableView registerNib:[UINib nibWithNibName:@"TempleSearchResultCell" bundle:nil] forCellReuseIdentifier:TEMPLE_SEARCH_RESULT_CELL];
    
    CGRect frame = _searchResultTableView.frame;
    frame.origin = CGPointMake(0, 215);
    frame.size.height = [Utility screenHeight] - 215;
    
    _searchResultMap = [Utility mapView1WithFrame:frame andDelegate:self andHasAd:_hasAd andFullscreen:NO];
    _searchResultMap.hidden = YES;
    
    [self.view addSubview:_searchResultMap];
    [self.view bringSubviewToFront:_pickerMaskView];
    
    [self startWorkProcess];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods

- (void)startWorkProcess
{
    _pickerShowFrame = _pickerMaskView.frame;
    _pickerHideFrame = _pickerShowFrame;
    _pickerHideFrame.origin.y = [Utility screenHeight];
    _pickerMaskView.frame = _pickerHideFrame;
    _pickerMaskView.hidden = YES;
    
    _selectedDistrict = 0;
    _selectedReligion = 0;
    _selectedDeity = 0;
    
    _database = [Database sharedInstance];
    [self buildDistrict];
    [self buildReligion];
    [self buildDeitiesBySelectedReligion];
    
    [self resetSearchResultCountView];
}

- (void)buildDistrict
{
    static NSString* columnName = @"district";
    
    NSMutableArray* dbDistricts = [_database getTempleColumnGroupByColumn:columnName where:@"WHERE district!='' AND valid_address>5" orderBy:@"ORDER BY `count` DESC"];
    
    _districts = [[NSMutableArray alloc] initWithArray:@[@{columnName:NSLocalizedString(@"全部地區", @"All districts"), @"count":@"0"}]];
    [_districts addObjectsFromArray:dbDistricts];
    [_districts addObject:@{columnName:NSLocalizedString(@"其他地區", @"Other district"), @"count":@"0"}];
}

- (void)buildReligion
{
    NSArray* dbReligions = [_database getTempleColumnGroupByColumn:@"religion" where:@"WHERE religion is not NULL AND valid_address>5" orderBy:@"ORDER BY `count` DESC"];
    _religions = [[NSMutableArray alloc] initWithArray:@[@{@"religion":NSLocalizedString(@"全部宗教", @"All religions"), @"count":@"0"}]];
    [_religions addObjectsFromArray:dbReligions];
    [_religions addObject:@{@"religion":NSLocalizedString(@"其他宗教", @"Other religion"), @"count":@"0"}];
}

- (void)buildDeitiesBySelectedReligion
{
    NSString* religionCondition = @"";
    
    if(_selectedReligion > 0 && _selectedReligion < [_religions count]-1)
    {
        NSDictionary* religion = [_religions objectAtIndex:_selectedReligion];
        NSString* religionName = [religion objectForKey:@"religion"];
        religionCondition = [NSString stringWithFormat:@"AND religion='%@'", religionName];
    }
    
    NSString* whereCondition = [NSString stringWithFormat:@"WHERE deity is not NULL AND valid_address>5 %@", religionCondition];
    
    NSArray* dbDeities = [_database getTempleColumnGroupByColumn:@"deity" where:whereCondition orderBy:@"ORDER BY `count` DESC"];
    _deities = [[NSMutableArray alloc] initWithArray:@[@{@"deity":NSLocalizedString(@"全部神祇", @"All deities"), @"count":@"0"}]];
    [_deities addObjectsFromArray:dbDeities];
    [_deities addObject:@{@"deity":NSLocalizedString(@"其他神祇", @"Other deity"), @"count":@"0"}];
}

- (void)resetSearchResultCountView
{
    NSString* selectedDistrictName = [self getSelectedFieldName:FIELD_DISTRICT];
    NSString* selectedReligionName = [self getSelectedFieldName:FIELD_RELIGION];
    NSString* selectedDeityName = [self getSelectedFieldName:FIELD_DEITY];
    
    _searchResultCount = [_database getTempleCountWithDistrict:selectedDistrictName andReligion:selectedReligionName andDeity:selectedDeityName andKeyword:_keywordTextField.text];
    [_searchResultCountLabel setTitle:[NSString stringWithFormat:@"%lu", (unsigned long)_searchResultCount] forState:UIControlStateNormal];
}

- (NSString*)getSelectedFieldName:(NSInteger)field
{
    NSString* selectedName;
    
    if(FIELD_DISTRICT == field)
    {
        if(_selectedDistrict > 0)
        {
            if(_selectedDistrict != [_districts count]-1)
            {
                NSDictionary* district = [_districts objectAtIndex:_selectedDistrict];
                selectedName = [district objectForKey:@"district"];
            }
            else
            {
                selectedName = @"";
            }
        }
    }
    else if(FIELD_RELIGION == field)
    {
        if(_selectedReligion > 0)
        {
            if(_selectedReligion != [_religions count]-1)
            {
                NSDictionary* religion = [_religions objectAtIndex:_selectedReligion];
                selectedName = [religion objectForKey:@"religion"];
            }
            else
            {
                selectedName = @"NULL";
            }
        }
    }
    else if(FIELD_DEITY == field)
    {
        if(_selectedDeity > 0)
        {
            if(_selectedDeity != [_deities count]-1)
            {
                NSDictionary* deity = [_deities objectAtIndex:_selectedDeity];
                selectedName = [deity objectForKey:@"deity"];
            }
            else
            {
                selectedName = @"NULL";
            }
        }
    }
    
    return selectedName;
}

- (void)showPicker
{
    if(!_pickerMaskView.hidden) // Already showed
    {
        return;
    }
    
    _pickerMaskView.hidden = NO;
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _pickerMaskView.frame = _pickerShowFrame;
    } completion:^(BOOL finished) {
    }];
}

- (void)hidePicker
{
    if(_pickerMaskView.hidden) // Already hidden
    {
        return;
    }
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _pickerMaskView.frame = _pickerHideFrame;
    } completion:^(BOOL finished) {
        _pickerMaskView.hidden = YES;
    }];
}

- (void)reportErrorTemple:(MapSpot*)spot
{
    if(spot.marker && spot.marker.userData)
    {
        Temple* temple = spot.marker.userData;
        [_database updateValidAddressWithId:temple.tid andValue:3];
        
        // Remove marker, path and hide callout from map
        spot.marker.map = nil;
        [_searchResultMap clearPath];
        [_searchResultMap hideCallout];
        
        [_searchResultMap removeSpot:spot];
    }
}

- (void)reportGrayMapTemple:(MapSpot*)spot
{
    if(spot.marker && spot.marker.userData)
    {
        Temple* temple = spot.marker.userData;
        [_database updateValidAddressWithId:temple.tid andValue:4];
        
        // Remove marker, path and hide callout from map
        spot.marker.map = nil;
        [_searchResultMap clearPath];
        [_searchResultMap hideCallout];
        
        [_searchResultMap removeSpot:spot];
    }
}

- (void)updateCoordinate:(MapSpot*)spot
{
    Temple* temple = spot.marker.userData;
    CLLocationCoordinate2D coordinate = [_searchResultMap getStreetViewCoordinate];
    
    if(CLLocationCoordinate2DIsValid(coordinate))
    {
        spot.marker.position = coordinate;
        [_database updateCoordinate:temple.tid toLatitude:coordinate.latitude andLongitude:coordinate.longitude];
    }
}

#pragma mark - UIPickerView datasource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(FIELD_DISTRICT == _currentPickerField)
    {
        return [_districts count];
    }
    else if(FIELD_RELIGION == _currentPickerField)
    {
        return [_religions count];
    }
    else if(FIELD_DEITY == _currentPickerField)
    {
        return [_deities count];
    }
    
    return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString* title = @"";
    
    if(FIELD_DISTRICT == _currentPickerField)
    {
        NSDictionary* district = [_districts objectAtIndex:row];
        title = [district objectForKey:@"district"];
        
        if([@"" isEqualToString:title])
        {
            title = NSLocalizedString(@"未知區域", @"Unknown district");
        }
        else
        {
            title = NSLocalizedString(title, @"District Name");
        }
    }
    else if(FIELD_RELIGION == _currentPickerField)
    {
        NSDictionary* religion = [_religions objectAtIndex:row];
        title = [religion objectForKey:@"religion"];
        
        if(!title)
        {
            title = NSLocalizedString(@"未知宗教", @"Unknown religion");
        }
    }
    else if(FIELD_DEITY == _currentPickerField)
    {
        NSDictionary* deity = [_deities objectAtIndex:row];
        title = [deity objectForKey:@"deity"];
        
        if(!title)
        {
            title = NSLocalizedString(@"未知神祇", @"Unknow deity");
        }
    }
    
    return title;
}

#pragma mark - UIPickerView delegate methods

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [Utility debug:@"PickerView select at row %d", row];
    
    if(FIELD_DISTRICT == _currentPickerField)
    {
        _selectedDistrict = row;
        
        NSDictionary* district = [_districts objectAtIndex:row];
        NSString* districtName = [district objectForKey:@"district"];
        
        [_districtButton setTitle:NSLocalizedString(districtName, @"distrct name in button") forState:UIControlStateNormal];
    }
    else if(FIELD_RELIGION == _currentPickerField)
    {
        _selectedReligion = row;
        
        // Reset deity data corresponding selected religion
        [self buildDeitiesBySelectedReligion];
        _selectedDeity = 0;
        NSDictionary* deity = [_deities objectAtIndex:_selectedDeity];
        NSString* deityName = [deity objectForKey:@"deity"];
        [_deityButton setTitle:deityName forState:UIControlStateNormal];
        
        // Update religion button title
        NSDictionary* religion = [_religions objectAtIndex:row];
        NSString* religionName = [religion objectForKey:@"religion"];
        
        [_religionButton setTitle:religionName forState:UIControlStateNormal];
    }
    else if(FIELD_DEITY == _currentPickerField)
    {
        _selectedDeity = row;
        
        NSDictionary* deity = [_deities objectAtIndex:row];
        NSString* deityName = [deity objectForKey:@"deity"];
        
        [_deityButton setTitle:deityName forState:UIControlStateNormal];
    }
    
    [_searchResultView setHidden:YES];
    [_searchResultMap setHidden:YES];
    [self resetSearchResultCountView];
}

#pragma mark - UITableView datasource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_searchResultTemples)
        return [_searchResultTemples count];
    else
        return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TempleSearchResultCell* cell = [tableView dequeueReusableCellWithIdentifier:TEMPLE_SEARCH_RESULT_CELL];
    
    NSInteger index = indexPath.row;
    Temple* temple = [_searchResultTemples objectAtIndex:index];

    cell.nameLabel.text = temple.chName;
    cell.religionLabel.text = temple.religion;
    cell.deityLabel.text = temple.deity;
    cell.addressLabel.text = temple.address;
    cell.starButton.hidden = YES;

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TempleSearchResultCell* cell = [tableView dequeueReusableCellWithIdentifier:TEMPLE_SEARCH_RESULT_CELL];
    return cell.frame.size.height;
}

#pragma mark - UITableView delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Utility debug:@"Selected at row %l", indexPath.row];
    
    [_searchResultMap switchMapMode:GMAP_MODE_MAP];
    [_searchResultMap tapMarkAtIndex:indexPath.row withZoom:13];
    [_modeSegmentControl setSelectedSegmentIndex:0];
    [self changeSearchPresentionMode:nil];
}

#pragma mark - Button event methods

- (IBAction)districtButtonTap:(id)sender
{
    [_keywordTextField endEditing:YES];
    _currentPickerField = FIELD_DISTRICT;
    [_pickerView reloadComponent:0];
    [_pickerView selectRow:_selectedDistrict inComponent:0 animated:NO];
    [self showPicker];
}

- (IBAction)religionButtonTap:(id)sender
{
    [_keywordTextField endEditing:YES];
    _currentPickerField = FIELD_RELIGION;
    [_pickerView reloadComponent:0];
    [_pickerView selectRow:_selectedReligion inComponent:0 animated:NO];
    [self showPicker];
}

- (IBAction)deityButtonTap:(id)sender
{
    [_keywordTextField endEditing:YES];
    _currentPickerField = FIELD_DEITY;
    [_pickerView reloadComponent:0];
    [_pickerView selectRow:_selectedDeity inComponent:0 animated:NO];
    [self showPicker];
}

- (IBAction)pickerOkButtonTap:(id)sender
{
//    [self searchButtonTap:nil];
    [self hidePicker];
}

- (IBAction)searchButtonTap:(id)sender
{
    _guideView.hidden = YES;
    [self doSearchWithProgress];
}

- (void)doSearchWithProgress
{
    [Utility debug:NSStringFromSelector(_cmd)];
    
    [_keywordTextField endEditing:YES];
    [self hidePicker];
    
    [_searchResultMap setHidden:NO];
    [_searchResultView setHidden:NO];
    
    // Update UI
    // clear all previous markers and path on map
    [_searchResultMap clearPath];
    [_searchResultMap clearAllSpot];
    
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode= MBProgressHUDModeDeterminate;
    hud.dimBackground = YES;
    hud.square = YES;
    hud.labelText = NSLocalizedString(@"載入資料中", @"Loading");
    hud.progress = 0;
    
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString* selectedDistrictName = [self getSelectedFieldName:FIELD_DISTRICT];
        NSString* selectedReligionName = [self getSelectedFieldName:FIELD_RELIGION];
        NSString* selectedDeityName = [self getSelectedFieldName:FIELD_DEITY];
        NSString* keyword = _keywordTextField.text;
        hud.progress = 0.06;
        
        _searchResultTemples = [_database getTempleWithDistrict:selectedDistrictName andReligion:selectedReligionName andDeity:selectedDeityName andKeyword:keyword];
        float initProgressDegree = 0.16;
        hud.progress = initProgressDegree;
        
        NSUInteger nTemple = [_searchResultTemples count];
        NSUInteger batchSize = nTemple / 33;
        batchSize = (batchSize < 10 ? 10 : batchSize);
        
        for(int i=0; i<nTemple; i+=batchSize)
        {
            // batch add markers to map in main thread
            NSUInteger endIndex = (i + batchSize > nTemple ? nTemple : i + batchSize);
            dispatch_sync(dispatch_get_main_queue(), ^{
                for(int j=i; j<endIndex; j++) {
                    Temple* t = [_searchResultTemples objectAtIndex:j];
                    [_searchResultMap addSpotWithTemple:t];
                }
            });
            
            hud.progress = (initProgressDegree + (1 - initProgressDegree) * endIndex / nTemple);
        }
        
        hud.progress = 1.0;
        
        hud.labelText = NSLocalizedString(@"更新中", @"Updating");
        hud.mode = MBProgressHUDModeIndeterminate;
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [_searchResultTableView reloadData];
            
            UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            hud.customView = imageView;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(@"完成", @"Done");
            
            [_searchResultMap fitBounds];
            [_searchResultMap switchMapMode:GMAP_MODE_MAP];
            [self resetSearchResultCountView];
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}
/*
- (void)doSearchWithIndicator
{
    [Utility debug:NSStringFromSelector(_cmd)];
    
    Utility* utility = [Utility sharedInstance];
    
    [_keywordTextField endEditing:YES];
    [self hidePicker];
    
    [_searchResultMap setHidden:NO];
    [_searchResultView setHidden:NO];
    
    [utility showHudOnWindowWithText:@"Searching" andMode:MBProgressHUDModeIndeterminate andTask:^{
    
        NSString* selectedDistrictName = [self getSelectedFieldName:FIELD_DISTRICT];
        NSString* selectedReligionName = [self getSelectedFieldName:FIELD_RELIGION];
        NSString* selectedDeityName = [self getSelectedFieldName:FIELD_DEITY];
        NSString* keyword = _keywordTextField.text;

        _searchResultTemples = [_database getTempleWithDistrict:selectedDistrictName andReligion:selectedReligionName andDeity:selectedDeityName andKeyword:keyword];
        
        // Update UI
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // clear all previous markers and path on map
            [_searchResultMap clearPath];
            [_searchResultMap clearAllSpot];
            [_searchResultTableView reloadData];
            
            NSUInteger nTemple = [_searchResultTemples count];
            
            for(int i=0; i<nTemple; i++)
            {
                Temple* t = [_searchResultTemples objectAtIndex:i];
                [_searchResultMap addSpotWithTemple:t];
            }
            
            [_searchResultMap fitBounds];
            [_searchResultMap switchMapMode:GMAP_MODE_MAP];
    
            [self resetSearchResultCountView];
        });
    
    } andCompletion:^{
        [Utility debug:@"Search Completion"];
    }];
}
*/
#pragma mark - Segmented Control methods

- (IBAction)changeSearchPresentionMode:(id)sender
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    NSInteger index = [sender selectedSegmentIndex];
    
    switch(index)
    {
        case 0:
            [Utility debug:@"顯示搜尋結果地圖"];
            _searchResultView.hidden = NO;
            _searchResultMap.hidden = NO;
            _searchResultTableView.hidden = YES;
            
            //
            [self.view bringSubviewToFront:_searchResultMap];
            break;
        case 1:
            [Utility debug:@"顯示搜尋結果列表"];
            _searchResultView.hidden = NO;
            _searchResultTableView.hidden = NO;
            _searchResultMap.hidden = YES;
            
            // Make table view can response touch
            [_searchResultView bringSubviewToFront:_searchResultTableView];
            [self.view bringSubviewToFront:_searchResultView];
            break;
        default:
            [Utility debug:@"No defined"];
    }
}

#pragma mark - MapViewDelegate methods

- (UIViewController*)getViewController
{
    return self;
}

- (void)penButtonTap:(MapSpot*)spot
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"功能選擇", @"Select function")
                                                        message:NSLocalizedString(@"移除：移除此廟宇地標。\r更新：更新為目前坐標。", @"Remove: remove the temple marker.\rUpdate: update the temple marker coordinate.")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"取消", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"移除", @"Remove"), NSLocalizedString(@"灰圖", @"Gray Map"), NSLocalizedString(@"更新", @"Update"), nil];
    alertView.tag = 100;
    objc_setAssociatedObject(alertView, @"map-spot", spot, OBJC_ASSOCIATION_RETAIN);
    [alertView show];
}

- (void)starButtonTap:(MapSpot*)spot enabled:(BOOL)enabled
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
    Temple* temple = spot.marker.userData;
    
    if(enabled)
    {
        temple.favorite = FAVORITE_LOVE;
#if FIX_DB_MODE
        // TODO: Remove this
        // Remove marker, path and hide callout from map
        spot.marker.map = nil;
        [_searchResultMap clearPath];
        [_searchResultMap hideCallout];
        [_searchResultMap removeSpot:spot];
#else
        spot.marker.icon = [MapView favoriteMarkerIcon];
#endif
    }
    else
    {
        temple.favorite = FAVORITE_NORMAL;
        spot.marker.icon = [Temple religionMarkerIcon:temple.religion];
    }
    
    [_database updateFavoriteWithId:temple.tid andValue:temple.favorite];
}

-(void)infoButtonTap:(MapSpot *)spot
{
    [Utility debug:@"%@", NSStringFromSelector(_cmd)];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_keywordTextField resignFirstResponder];
    return YES;
}

- (IBAction)keywordTextFieldDidBeginEdit:(id)sender
{
    [self hidePicker];
}

- (IBAction)keywordValueChanged:(id)sender
{
    [Utility debug:@"value changed %@", _keywordTextField.text];
    [self resetSearchResultCountView];
}

#pragma mark - AlertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100)
    {
        MapSpot* spot = objc_getAssociatedObject(alertView, @"map-spot");
        
        if(spot)
        {
            if(buttonIndex == 1) // 移除
            {
                [self reportErrorTemple:spot];
            }
            else if(buttonIndex == 2)
            {
                [self reportGrayMapTemple:spot];
            }
            else if(buttonIndex == 3) // 更新
            {
                [self updateCoordinate:spot];
            }
        }
    }
}

#pragma mark - Touches methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_keywordTextField endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
