//
//  Temple.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/11.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define FAVORITE_LOVE   10
#define FAVORITE_NORMAL  5

@interface Temple : NSObject

@property (assign, nonatomic) NSUInteger tid;
@property (strong, nonatomic) NSString* seq;
@property (strong, nonatomic) NSString* chName;
@property (strong, nonatomic) NSString* enName;
@property (strong, nonatomic) NSString* deity;
@property (strong, nonatomic) NSString* district;
@property (strong, nonatomic) NSString* origAddress;
@property (strong, nonatomic) NSString* address;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (strong, nonatomic) NSString* religion;
@property (strong, nonatomic) NSString* tel;
@property (strong, nonatomic) NSString* chargePerson;
@property (assign, nonatomic) NSUInteger validAddress;
@property (assign, nonatomic) NSInteger favorite;

+(Temple*)copy:(Temple*)temple;
+(NSDictionary*)religionColors;
+(NSDictionary*)religionMarkerIcons;
+(UIColor*)religionColor:(NSString*)religionName;
+(UIImage*)religionMarkerIcon:(NSString*)religionName;
+(BOOL)isValidAddress:(NSString*)address;

-(NSString*)toString;

@end