//
//  Temple.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/11.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "Temple.h"
#import "Utility.h"
#import "Database.h"

@interface Temple ()

@end

@implementation Temple

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(NSString*)toString
{
    NSString* result = [NSString stringWithFormat:@"tid=%d, seq=%@, chName=%@, enName%@, deity=%@, religion=%@, district=%@, origAddress=%@, address=%@, (%f, %f), tel=%@, chargePerson=%@, isValidAddress=%d, favorite=%d",
                        _tid, _seq, _chName, _enName, _deity, _religion, _district, _origAddress, _address, _latitude, _longitude, _tel, _chargePerson, _validAddress, _favorite];
    return result;
}

#pragma mark - class methods

+(Temple*)copy:(Temple*)temple
{
    Temple* t = [[Temple alloc] init];
    t.tid = temple.tid;
    t.seq = temple.seq;
    t.chName = temple.chName;
    t.enName = temple.enName;
    t.deity = temple.deity;
    t.district = temple.district;
    t.origAddress = temple.origAddress;
    t.address = temple.address;
    t.latitude = temple.latitude;
    t.longitude = temple.longitude;
    t.religion = temple.religion;
    t.tel = temple.tel;
    t.chargePerson = temple.chargePerson;
    t.validAddress = temple.validAddress;
    t.favorite = temple.favorite;
    
    return t;
}

+(NSDictionary*)religionColors
{
    static NSMutableDictionary* colors = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        // Initialize religion marker colors
        Database* db = [Database sharedInstance];
        NSString* columnName = @"religion";
        NSArray* religions = [db getTempleColumnGroupByColumn:columnName where:@"" orderBy:@"ORDER BY `count` DESC"];
        
        for(NSDictionary* r in religions)
        {
            NSString* name = [r objectForKey:columnName];
            NSNumber* count = [r objectForKey:@"count"];
            [Utility debug:@"%@ %@", name, count];
        }
        
        colors = [[NSMutableDictionary alloc] init];
        
        NSUInteger n = [religions count];
        for(int i=0; i<n; i++)
        {
            NSDictionary* religion = [religions objectAtIndexedSubscript:i];
            NSString* religionName = [religion objectForKey:@"religion"];
            
            if(religionName)
            {
                UIColor* color = [UIColor colorWithHue:((float)(1.0f/i)) saturation:1 brightness:1 alpha:1];
                [colors setValue:color forKey:religionName];
            }
        }
    });
    
    return colors;
}

+(NSDictionary*)religionMarkerIcons
{
    static NSMutableDictionary* icons = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{

        // Initialize religion marker icons
        Database* db = [Database sharedInstance];
        NSString* columnName = @"religion";
        NSArray* religions = [db getTempleColumnGroupByColumn:columnName where:@"" orderBy:@"ORDER BY `count` DESC"];
        
        NSDictionary* colors = [Temple religionColors];
        icons = [[NSMutableDictionary alloc] init];
        
        NSUInteger n = [religions count];
        for(int i=0; i<n; i++)
        {
            NSDictionary* religion = [religions objectAtIndexedSubscript:i];
            NSString* religionName = [religion objectForKey:@"religion"];
            
            if(religionName)
            {
                UIImage* icon = [GMSMarker markerImageWithColor:[colors objectForKey:religionName]];
                [icons setValue:icon forKey:religionName];
            }
        }
    });
    
    return icons;
}

+(UIColor*)religionColor:(NSString*)religionName
{
    NSDictionary* colors = [Temple religionColors];
    
    if(!colors)
    {
        [Utility error:@"Religion colors haven't initialized !"];
        return nil;
    }
    
    return [colors objectForKey:religionName];
}

+(UIImage*)religionMarkerIcon:(NSString*)religionName
{
    NSDictionary* icons = [Temple religionMarkerIcons];
    
    if(!icons)
    {
        [Utility error:@"Religion icons haven't initialized !"];
        return nil;
    }
    
    return [icons objectForKey:religionName];
}

+(BOOL)isValidAddress:(NSString*)address
{
    static NSPredicate* predictMatch;
    
    if(!predictMatch)
        predictMatch = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @".+[路街].+"];
    
    return [predictMatch evaluateWithObject:address];
}

+(NSArray*)extraInvalidTemples
{
    NSArray* invalidTempleSeq = @[@"",
                                  @""];
    
    return invalidTempleSeq;
}

@end