//
//  TempleSearchResultCell.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/22.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Temple.h"

@class TempleSearchResultCell;

@protocol TempleCellEventDelegate
@optional
- (void)starButtonStateChanged:(TempleSearchResultCell*)cell andEnabled:(BOOL)enabled;
@end

@interface TempleSearchResultCell : UITableViewCell

+ (UIImage*)starShineImage;
+ (UIImage*)starEmptyImage;

@property (weak, nonatomic) id<TempleCellEventDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *religionLabel;
@property (strong, nonatomic) IBOutlet UILabel *deityLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UIImageView *mapImage;
@property (strong, nonatomic) IBOutlet UIButton *starButton;

@property (strong, nonatomic) Temple* data;
@property (assign, nonatomic) BOOL isStarButtonEnabled;

- (void)enableStartButton:(BOOL)enabled;

@end
