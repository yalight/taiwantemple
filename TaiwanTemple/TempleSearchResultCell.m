//
//  TempleSearchResultCell.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/22.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "TempleSearchResultCell.h"
#import "Utility.h"

@implementation TempleSearchResultCell

#pragma mark - Class methods

+ (UIImage*)starShineImage
{
    static UIImage* starImage;
    
    if(!starImage)
    {
        starImage = [UIImage imageNamed:@"star-shine-icon"];
    }
    
    return starImage;
}

+ (UIImage*)starEmptyImage
{
    static UIImage* starImage;
    
    if(!starImage)
    {
        starImage = [UIImage imageNamed:@"star-outline-icon"];
    }
    
    return starImage;
}

#pragma mark - Private methods

- (void)awakeFromNib
{
    // Initialization code
    _isStarButtonEnabled = YES;
    
    // For make button can reponsive tap action in UITableCellView
    // See http://stackoverflow.com/questions/18848689/button-in-uitableviewcell-not-responding-under-ios-7
    [self bringSubviewToFront:_starButton];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Buttons handle methods

- (IBAction)starButtonTap:(id)sender
{
    [Utility debug:@"star tap at %d", _data.tid];
    [self enableStartButton:!_isStarButtonEnabled];
    
    if(_delegate)
    {
        [_delegate starButtonStateChanged:self andEnabled:_isStarButtonEnabled];
    }
}

#pragma mark - Public methods

- (void)enableStartButton:(BOOL)enabled
{
    if(enabled)
    {
        [_starButton setImage:TempleSearchResultCell.starShineImage forState:UIControlStateNormal];
    }
    else
    {
        [_starButton setImage:TempleSearchResultCell.starEmptyImage forState:UIControlStateNormal];
    }
    
    _isStarButtonEnabled = enabled;
}


@end
