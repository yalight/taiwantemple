//
//  TempleStructure.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/21.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TempleStructure : NSObject

@property (assign, nonatomic) NSUInteger sid;
@property (strong, nonatomic) NSString* chName;
@property (strong, nonatomic) NSString* html;
@property (strong, nonatomic) NSString* url;

- (NSString*)toString;

@end
