//
//  TempleStructure.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/21.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "TempleStructure.h"

@implementation TempleStructure

- (NSString*)toString
{
    return [NSString stringWithFormat:@"sid=%lu, chName=%@", (unsigned long)_sid, _chName];
}

@end
