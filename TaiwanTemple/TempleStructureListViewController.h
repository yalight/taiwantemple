//
//  TempleStructureViewController.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/21.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TempleStructureListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
