//
//  TempleStructureViewController.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/21.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "TempleStructureListViewController.h"
#import "TempleStructureViewController.h"
#import "Database.h"

#define STRUCTURE_CELL_ID @"STRUCTURE_CELL_ID"

@interface TempleStructureListViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) Database* database;
@property (strong, nonatomic) NSMutableArray* structures;

@end

@implementation TempleStructureListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)initSetup
{
    _database = [Database sharedInstance];
    
    _structures = [_database getAllTempleStructures];
    
//    _structures = @[@"屋頂", @"脊飾", @"燕尾脊", @"牌頭", @"剪黏", @"山牆", @"磬牌", @"鳥踏", @"馬背", @"瓦當與滴水", @"龍柱", @"石獅", @"石鼓", @"門枕石"];
    
//    [_tableView registerNib:[UINib nibWithNibName:@"TempleSearchResultCell" bundle:nil] forCellReuseIdentifier:STRUCTURE_CELL_ID];
}

#pragma mark - UITableView datasource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_structures count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:STRUCTURE_CELL_ID];
    
    TempleStructure* structure = [_structures objectAtIndex:indexPath.row];
    cell.textLabel.text = structure.chName;
    cell.tag = indexPath.row;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([@"temple_structure_segue" isEqualToString:segue.identifier])
    {
        TempleStructureViewController* vc = [segue destinationViewController];
        TempleStructure* structure = [_structures objectAtIndex:[(UITableViewCell*)sender tag]];
        [Utility debug:@"%@", [structure toString]];
        vc.structure = structure;
    }
}

@end
