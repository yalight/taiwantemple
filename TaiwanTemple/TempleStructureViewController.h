//
//  TempleStructureViewController.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/21.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempleStructure.h"

@interface TempleStructureViewController : UIViewController

@property (strong, nonatomic) TempleStructure* structure;

@end
