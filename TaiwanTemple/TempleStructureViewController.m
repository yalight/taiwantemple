//
//  TempleStructureViewController.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/10/21.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "TempleStructureViewController.h"
#import "Utility.h"

@interface TempleStructureViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;

@end

@implementation TempleStructureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(HAS_AD)
        [[Utility sharedInstance] setupAdModBanner:_bannerView andRootViewController:self];
    
    self.title = _structure.chName;
    
    //load url into webview
//    NSString *strURL = _structure.url;
//    NSURL *url = [NSURL URLWithString:strURL];
//    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
//    [_webview loadRequest:urlRequest];
    
    NSString* htmlString = _structure.html;
    [_webview loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
