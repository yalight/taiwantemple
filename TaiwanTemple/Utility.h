//
//  Utility.h
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/11.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "MapView.h"
#import "GADBannerView.h"
#import "GADRequest.h"

#define LOG_LEVEL_DEBUG   1
#define LOG_LEVEL_INFO    2
#define LOG_LEVEL_WARNING 3
#define LOG_LEVEL_ERROR   4

@interface Utility : NSObject

+(instancetype)sharedInstance;

+ (MapView*)mapView1WithFrame:(CGRect)frame andDelegate:(id<MapViewDelegate>)delegate andHasAd:(BOOL)hasAd andFullscreen:(BOOL)isFullscreen;
+ (MapView*)mapView2WithFrame:(CGRect)frame andDelegate:(id<MapViewDelegate>)delegate andHasAd:(BOOL)hasAd;
+(CGFloat)screenWidth;
+(CGFloat)screenHeight;

+(void)info:(NSString*)formatString, ...;
+(void)debug:(NSString*)formatString, ...;
+(void)warn:(NSString*)formatString, ...;
+(void)error:(NSString*)formatString, ...;

+ (UIImage*)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

- (void)setupAdModBanner:(GADBannerView*)bannerView andRootViewController:(UIViewController*)rootViewController;

//- (void)showHudOnWindowWithText:(NSString*)text andMode:(MBProgressHUDMode)mode andTask:(void(^)())block andCompletion:(void(^)())completion;
//- (void)showHudOnWindowWithText:(NSString*)text andMode:(MBProgressHUDMode)mode andSelector:(SEL)selector andTarget:(id)target;
//- (void)hideHud;

@end