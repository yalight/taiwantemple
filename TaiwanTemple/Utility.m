//
//  Utility.m
//  TaiwanTemple
//
//  Created by HDD103033 on 2014/9/11.
//  Copyright (c) 2014年 Yalight. All rights reserved.
//

#import "Utility.h"

@interface Utility ()
@property (strong, nonatomic) MBProgressHUD* HUD;
@end

@implementation Utility

#pragma mark - init

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - Singleton methods

+ (instancetype)sharedInstance
{
    static Utility* sharedUtility = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedUtility = [[self alloc] init];
    });
    
    return sharedUtility;
}

#pragma mark - Class methods

+ (MapView*)mapView1WithFrame:(CGRect)frame andDelegate:(id<MapViewDelegate>)delegate andHasAd:(BOOL)hasAd andFullscreen:(BOOL)isFullscreen
{
    static MapView* mapView;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        mapView = [[MapView alloc] initWithFrame:frame];
    });
    
    [mapView removeFromSuperview];
    [mapView setDelegate:delegate];
    [mapView setHasAd:hasAd];
    [mapView setIsFullScreen:isFullscreen];
    [mapView setFrame:frame];
    [mapView clearAllSpot];
    [mapView switchMapMode:GMAP_MODE_MAP];
    [mapView setHidden:NO];
    
    return mapView;
}

+ (MapView*)mapView2WithFrame:(CGRect)frame andDelegate:(id<MapViewDelegate>)delegate andHasAd:(BOOL)hasAd
{
    static MapView* mapView;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        mapView = [[MapView alloc] initWithFrame:frame];
    });
    
    [mapView removeFromSuperview];
    [mapView setDelegate:delegate];
    [mapView setHasAd:hasAd];
    [mapView setFrame:frame];
    [mapView clearAllSpot];
    [mapView switchMapMode:GMAP_MODE_MAP];
    [mapView setHidden:NO];
    
    return mapView;
}

+ (CGFloat)screenWidth
{
    static CGFloat width = -1;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        width = [UIScreen mainScreen].bounds.size.width;
    });
    
    return width;
}

+ (CGFloat)screenHeight
{
    static CGFloat height = -1;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        height = [UIScreen mainScreen].bounds.size.height;
    });
    
    return height;
}

+ (void)info:(NSString*)formatString, ...
{
    va_list args;
    va_start(args, formatString);
    NSString* message = [[NSString alloc] initWithFormat:formatString arguments:args];
    va_end(args);
    
    NSLog(@"[INFO] %@", message);
}

+ (void)debug:(NSString*)formatString, ...
{
    va_list args;
    va_start(args, formatString);
    NSString* message = [[NSString alloc] initWithFormat:formatString arguments:args];
    va_end(args);
    
    NSLog(@"[DEBUG] %@", message);
}

+ (void)warn:(NSString*)formatString, ...
{
    va_list args;
    va_start(args, formatString);
    NSString* message = [[NSString alloc] initWithFormat:formatString arguments:args];
    va_end(args);
    
    NSLog(@"[WARN] %@", message);
}

+ (void)error:(NSString*)formatString, ...
{
    va_list args;
    va_start(args, formatString);
    NSString* message = [[NSString alloc] initWithFormat:formatString arguments:args];
    va_end(args);
    
    NSLog(@"[ERROR] %@", message);
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Public methods

- (void)setupAdModBanner:(GADBannerView*)bannerView andRootViewController:(UIViewController*)rootViewController
{
#if DEBUG
    static NSString* adUnitId = @"ca-app-pub-3940256099942544/2934735716";
#else
    static NSString* adUnitId = @"ca-app-pub-5407542036815499/9681270768";
#endif
    
    bannerView.adUnitID = adUnitId;
    bannerView.rootViewController = rootViewController;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}

//- (void)showHudOnWindowWithText:(NSString*)text andMode:(MBProgressHUDMode)mode andTask:(void(^)())block andCompletion:(void(^)())completion
//{
//    [Utility debug:@"Show HUD"];
//    
//    if(!_HUD)
//    {
//        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
//        _HUD = [[MBProgressHUD alloc] initWithWindow:window];
//        [window addSubview:_HUD];
//    }
//    
//    _HUD.labelText = text;
//    _HUD.dimBackground = YES;
//    _HUD.mode = mode;
//    
//    [_HUD showAnimated:YES whileExecutingBlock:^{
//        block();
//    } completionBlock:^{
//        if(completion)
//            completion();
//    }];
//}
//
//- (void)showHudOnWindowWithText:(NSString*)text andMode:(MBProgressHUDMode)mode andSelector:(SEL)selector andTarget:(id)target
//{
//    [Utility debug:@"Show HUD"];
//    
//    if(!_HUD)
//    {
//        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
//        _HUD = [[MBProgressHUD alloc] initWithWindow:window];
//        [window addSubview:_HUD];
//    }
//    
//    _HUD.labelText = text;
//    _HUD.dimBackground = YES;
//    _HUD.mode = mode;
//    
//    [_HUD showWhileExecuting:selector onTarget:target withObject:nil animated:YES];
//}
//
//- (void)hideHud
//{
//    if(_HUD)
//    {
//        [_HUD hide:YES];
//    }
//}

@end